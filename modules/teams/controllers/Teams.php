<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Teams extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('teams_model');
    }


    public function index()
    {
        if($this->staff_model->getTeams() == '1'){
            access_denied('Teams');
        }
        
        $this->app_scripts->add('circle-progress-js','assets/plugins/jquery-circle-progress/circle-progress.min.js');
        $data['title'] = 'teams';
        $data['team_tabs'] = $this->teams_model->get_teams();


        foreach($data['team_tabs'] as $teams){
            $data['team_sum'][] = array(
                'name' => $teams->name,
                'data' => $this->teams_model->get_teams_summary($teams->teamid),
                'users_count' => $this->teams_model->get_team_users($teams->teamid),
                'teamid' => $teams->teamid
            );
        }
        $this->load->view('manage', $data);
    }

    public function get_team($id = 1){
        if ($this->input->is_ajax_request()) {
            $tdata['teamid'] = $id;
            $currentUser = $this->staff_model->get(get_staff_user_id());
            $my_timezone = $currentUser->user_timezone;
            if($my_timezone == NULL){
                $tdata['user_timezone'] = 'Asia/Manila';
            }else{
                $tdata['user_timezone'] = $my_timezone;
            }

            $this->app->get_table_data(module_views_path('teams', 'table'),$tdata);
        }
    }

    public function team_member($teamid)
    {
        echo json_encode([
            'success' => $this->misc_model->get_tasks_distinct_unassignees($teamid)
        ]);
    }

    public function set_team(){
        $data = ['teamid' => $this->input->post('teamid')];
        $this->db->where('staffid', $this->input->post('staffid'));
        if($this->db->update(db_prefix().'staff',$data)){
            return true;
        }              
    }

    public function get_team_color($teamid){
        if($teamid==1){
            return 'red';
        }else if($teamid==2){
            return 'blue';
        }else if($teamid==3){
            return 'green';
        }else if($teamid==4){
            return 'orange';
        }else if($teamid==5){
            return 'yellow';
        }else if($teamid==6){
            return 'brown';
        }else if($teamid==7){
            return 'purple';
        }else{
            return 'black';
        }
    }

    public function gantt()
    {
        $this->app_css->add('jquery-gantt-css','assets/plugins/gantt/css/style.css?v=2.4.4');
        $this->app_css->add('custom-css','assets/css/custom.css');        
        $this->app_css->add('app-css','assets/css/style.min.css?v=1588943054');
        $this->app_scripts->add('jquery-gantt-js','assets/plugins/gantt/js/jquery.fn.gantt.min.js?v=2.4.4');
        $data['gantt_data'] = [];
        
        $appliedMember   = $this->input->get('member');
        $team   = $this->input->get('team');
        $dates   = $this->input->get('dates');
        $status = $this->input->get('status');


        $data['dates'] = $this->input->get('dates');
        $data['statusaa'] = $this->input->get('status');
        $data['team'] = $this->input->get('team');
        $data['appliedMember']   = $this->input->get('member');

        $data['team_tabs'] = $this->teams_model->get_teams();
        $data['project_members'] = $this->staff_model->get('', ['active' => 1]);

        $data['gantt_data'] = $this->teams_model->get_gantt_tasks($team, $appliedMember, $dates, $status);
        
        $this->load->view('gantt', $data);
    }



    

    public function goal($id = '')
    {
        if (!has_permission('goals', '', 'view')) {
            access_denied('goals');
        }
        if ($this->input->post()) {
            if ($id == '') {
                if (!has_permission('goals', '', 'create')) {
                    access_denied('goals');
                }
                $id = $this->goals_model->add($this->input->post());
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('goal')));
                    redirect(admin_url('goals/goal/' . $id));
                }
            } else {
                if (!has_permission('goals', '', 'edit')) {
                    access_denied('goals');
                }
                $success = $this->goals_model->update($this->input->post(), $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('goal')));
                }
                redirect(admin_url('goals/goal/' . $id));
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('goal_lowercase'));
        } else {
            $data['goal']        = $this->goals_model->get($id);
            $data['achievement'] = $this->goals_model->calculate_goal_achievement($id);

            $title = _l('edit', _l('goal_lowercase'));
        }

        $this->load->model('staff_model');
        $data['members'] = $this->staff_model->get('', ['is_not_staff' => 0, 'active'=>1]);

        $this->load->model('contracts_model');
        $data['contract_types']        = $this->contracts_model->get_contract_types();
        $data['title']                 = $title;
        $this->app_scripts->add('circle-progress-js','assets/plugins/jquery-circle-progress/circle-progress.min.js');
        $this->load->view('goal', $data);
    }

    /* Delete announcement from database */
    public function delete($id)
    {
        if (!has_permission('goals', '', 'delete')) {
            access_denied('goals');
        }
        if (!$id) {
            redirect(admin_url('goals'));
        }
        $response = $this->goals_model->delete($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('goal')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('goal_lowercase')));
        }
        redirect(admin_url('goals'));
    }

    public function notify($id, $notify_type)
    {
        if (!has_permission('goals', '', 'edit') && !has_permission('goals', '', 'create')) {
            access_denied('goals');
        }
        if (!$id) {
            redirect(admin_url('goals'));
        }
        $success = $this->goals_model->notify_staff_members($id, $notify_type);
        if ($success) {
            set_alert('success', _l('goal_notify_staff_notified_manually_success'));
        } else {
            set_alert('warning', _l('goal_notify_staff_notified_manually_fail'));
        }
        redirect(admin_url('goals/goal/' . $id));
    }
}
