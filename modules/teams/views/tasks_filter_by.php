<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="_hidden_inputs _filters _tasks_filters1 ">
    <?php
        $task_statuses = $this->teams_model->get_statuses();
        $tasks_filter_assignees = $this->misc_model->get_tasks_distinct_unassignees();
        $all_member = $this->misc_model->all_member();

        foreach($task_statuses as $status){
            $val = 'true';
            if($status['filter_default'] == false){
                $val = '';
            }
            echo form_hidden('task_status_'.$status['id'],$val);
        }

        if(has_permission('tasks','','view')){
            foreach($all_member as $tf_assignee){
                echo form_hidden('task_assigned_'.$tf_assignee['assigneeid']);
            }
        }

    ?>
    <input type="hidden" name="member_no_tasks">
</div>

<div class="btn-group pull-right mleft4 mbot25 btn-with-tooltip-group _filter_data" data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
       <i class="fa fa-filter" aria-hidden="true"></i>
   </button>
   <ul class="dropdown-menu width300">
        <li>
            <a href="#" id="all" data-cview="all" onclick="dt_custom_view('','<?php echo $view_table_name; ?>',''); return false;">
                <?php echo _l('task_list_all'); ?>
            </a>
        </li>
        <li class="divider"></li>
        <?php foreach($task_statuses as $status){ ?>
        <li class="clear-all-prevent<?php if($status['filter_default'] == true ){echo ' active';} ?>" >
        <!-- <li class="filter-group" > -->
            <a href="#" id="task_status_<?php echo $status['id']; ?>" data-cview="task_status_<?php echo $status['id']; ?>" onclick="dt_custom_view('task_status_<?php echo $status['id']; ?>','<?php echo $view_table_name; ?>','task_status_<?php echo $status['id']; ?>'); return false;">
                <?php echo $status['name']; ?>
            </a>
        </li>
        <?php } ?>
        
        
        
        <div class="clearfix"></div>
        <li class="divider"></li>
        <li class="filter-group" data-filter-group="filter-group">
            <a href="#" id="member_no_tasks" data-cview="member_no_tasks" onclick="dt_custom_view('member_no_tasks','<?php echo $view_table_name; ?>','member_no_tasks'); return false;">
                Members With No Active Task
            </a>
        </li>
        <li class="dropdown-submenu pull-left">
            <a href="#" tabindex="-1" >By Member</a>
            <ul class="dropdown-menu dropdown-menu-left" id="assigned">
                <?php foreach($tasks_filter_assignees as $as){ ?>
                <li>
                    <a href="#" data-cview="task_assigned_<?php echo $as['assigneeid']; ?>" onclick="dt_custom_view(<?php echo $as['assigneeid']; ?>,'<?php echo $view_table_name; ?>','task_assigned_<?php echo $as['assigneeid']; ?>'); return false;"><?php echo $as['full_name']; ?></a>
                </li>
                <?php } ?>
            </ul>
        </li>
        
        
    </ul>
</li>





</ul>
</div>

