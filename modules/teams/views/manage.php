<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                     <div class="row _buttons">
                        <div class="col-md-8">
                            <a href="<?php echo admin_url('teams/gantt'); ?>" class="btn btn-info pull-left display-block">GANTT VIEW</a>
                            <!-- put gant view link here -->
                        </div>
                        <div class="col-md-4">
                            <?php $this->load->view('teams/tasks_filter_by',array('view_table_name'=>'.table-teams_1')); ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr-panel-heading" />
                    <h4 class="mbot15">Teams Summary</h4>
                    <div>
                        <ul class="nav nav-tabs nav-tabs-horizontal" role="tablist">
                            <?php foreach($team_sum as $teams): ?>
                            <li role="presentation" <?php if($teams['teamid'] == 1){ ?>class=" active"<?php } ?>>
                                <a teamid="<?php echo $teams['teamid']; ?>" fetch_url="<?php echo base_url('admin/teams/get_team/'.$teams['teamid']); ?>" href="#team_tab_<?php echo $teams['teamid']; ?>" aria-controls="team_tab_<?php echo $teams['teamid']; ?>" role="tab" data-toggle="tab" aria-expanded="false">
                                    <i class="fa fa-tasks menu-icon"></i><?php echo $teams['name']; ?>
                                </a>
                                <p style="color:#989898; text-align: center; font-size: 12px !important" class="font-medium-xs no-mbot">
                                    USERS
                                    <br>
                                    <?php echo $teams['users_count']; ?>
                                </p>
                                
                                <p class="font-medium-xs no-mbot text-muted" style="text-align: center; font-size: 12px !important;">
                                    Tasks
                                    <br/>
                                    <?php echo count($teams['data']); ?>
                                </p>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="tab-content">
                        
                            <?php foreach($team_tabs as $teams): ?>
                            <div role="tabpanel" class="tab-pane <?php if($teams->teamid == 1){ ?>active<?php } ?>" id="team_tab_<?php echo $teams->teamid; ?>">
                                <?php 
                                    $table_data = [
                                        "ID","STAFF","TASK","STATUS",'TYPE','TASK CREATION DATE','DUE DATE', 'tasksid'
                                    ];
                                
                                    render_datatable($table_data,'teams_'.$teams->teamid);
                                ?>
                            </div>
                            <?php endforeach; ?>
                            
                        </div>
                    </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php init_tail(); ?>
<script>
    $(function(){
        var teamsStatus = {};

        $.each($('._hidden_inputs._filters input'),function(){
            teamsStatus[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
        });

        initDataTable('.table-teams_1', '<?php echo base_url('admin/teams/get_team/1'); ?>', undefined,undefined, teamsStatus, '', 1);
        $('.nav-tabs').on("shown.bs.tab",function(event){

            var target = $(event.target).attr("fetch_url") // activated tab
            var teamid = $(event.target).attr("teamid")

            $.each($('._hidden_inputs._filters input'),function(){
                $("#"+$(this).attr('name')).attr("onclick","dt_custom_view('"+$(this).attr('name')+"', '.table-teams_"+teamid+"', '"+$(this).attr('name')+"')" );
            });

            $("#all").attr("onclick","dt_custom_view('', '.table-teams_"+teamid+"', '')" );
            $("#member_no_tasks").attr("onclick","dt_custom_view('member_no_tasks', '.table-teams_"+teamid+"', 'member_no_tasks')" );
            
            requestGetJSON('/teams/team_member/' + teamid).done(function(response) {
                $('._hidden_inputs._filters').html('');
                $('#assigned').html(' ');
                $('._hidden_inputs._filters').append('<input type="hidden" name="task_status_1" value="">');
                $('._hidden_inputs._filters').append('<input type="hidden" name="task_status_2" value="">');
                $('._hidden_inputs._filters').append('<input type="hidden" name="task_status_3" value="">');
                $('._hidden_inputs._filters').append('<input type="hidden" name="task_status_4" value="">');
                $('._hidden_inputs._filters').append('<input type="hidden" name="task_status_5" value="">');
                $('._hidden_inputs._filters').append('<input type="hidden" name="member_no_tasks" value="">');
                $.each(response['success'],function(y, x){
                    $('._hidden_inputs._filters').append('<input type="hidden" name="task_assigned_'+x['assigneeid']+'">');
                    var table = "'.table-teams_"+teamid+"'"; 
                    var assigned = "'task_assigned_"+x['assigneeid']+"'"; 

                    $('#assigned').append('<li><a href="#" data-cview="task_assigned_'+x['assigneeid']+'" onclick="dt_custom_view('+x['assigneeid']+', '+table+', '+assigned+' ); return false;">'+x['full_name']+'</a></li>');
                });
                
            });
            // ".table-teams_'+teamid+'", "task_assigned_'+x['assigneeid']+'"
            initDataTable('.table-teams_'+teamid, target, [0],[0], teamsStatus, '', 1);
            $('.table-teams_'+teamid).DataTable().on('draw', function() {        

            });
        });
    });


</script>
</body>
</html>
