<?php

defined('BASEPATH') or exit('No direct script access allowed');
$CI          = & get_instance();
$__post      = $CI->input->post();

$aColumns = [db_prefix() .'staff.staffid','CONCAT(firstname," ",lastname)',db_prefix() .'tasks.name','status','rel_type','dateadded','duedate', 'tbltasks.id'];
$sIndexColumn = 'staffid';
$sTable       = db_prefix() . 'staff';


$statusIds = [];
$filter = [];
$assignees = [];
$member_no_tasks = '';

foreach ($this->ci->teams_model->get_statuses() as $status) {
    if ($this->ci->input->post('task_status_' . $status['id'])) {
        array_push($statusIds, $status['id']);
    }
}

foreach ($this->ci->misc_model->all_member() as $status) {
    if ($this->ci->input->post('task_assigned_' . $status['assigneeid'])) {
        array_push($assignees, $status['assigneeid']);
    }
}

if($this->ci->input->post('member_no_tasks')) {
    $member_no_tasks = 4;
}


$join = [
    'LEFT JOIN ' . db_prefix() . 'task_assigned ON ' . db_prefix() . 'staff.staffid = ' . db_prefix() . 'task_assigned.staffid '.
    'LEFT JOIN '.db_prefix() . 'tasks ON '.db_prefix().'task_assigned.taskid = '.db_prefix().'tasks.id '
];

if ((isset($__post['search'])) && $__post['search']['value'] != '') {
    $where = [' AND tblstaff.teamid = '.$teamid. ' AND tblstaff.teamid != 0'];
}else{   
    $where = ['WHERE tblstaff.teamid = '.$teamid.' AND tblstaff.teamid != 0 '];
}

$checkAssignees = implode(', ',$assignees);
if($checkAssignees != ''){
    array_push($where, ' AND tblstaff.staffid in ('. $checkAssignees .') ');
} 

if (count($statusIds) > 0) {
    array_push($where, ' AND tbltasks.status in ('. implode(', ',$statusIds) .') ');
}

if($member_no_tasks == 4){
    array_push($where, ' AND tbltasks.status != 4 ');
}

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [db_prefix() .'staff.staffid']);

$output  = $result['output'];

$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $tasksid = $aRow['tbltasks.id'];
    $row = [];
    for ($i = 0; $i < count($aColumns); $i++) {
        $_data = $aRow[$aColumns[$i]];
        if($i == 1){
            $_data = '<a class="display-block main-tasks-table-href-name mbot5" href="' . admin_url('staff/member/' . $aRow['staffid']) . '">' . $_data . '</a>';
            //$_data .= '<span class="hide"> - </span>';
            //$_data .= '<a class="text-muted task-table-related" href="' . admin_url('goals/goal/' . $aRow['staffid']) . '">' . _l('view') . '</a>';
        }else if($i == 2){
            $_data = '<a class="display-block main-tasks-table-href-name mbot5" href="' . admin_url('tasks/view/' . $tasksid) . '" onclick="init_task_modal('.$tasksid.'); return false;">' . $_data . '</a>';
        }else if($i == 3){
            if($_data == 4){
                $_data = 'In Progress';
            }else if($_data == 3){
                $_data = 'Testing';                  
            }else if($_data == 2){
                $_data = 'Awaiting Feedback';                
            }else if($_data == 1){
                $_data = 'Not Started';                
            }else if($_data == 5){
                $_data = 'Complete';                
            }
        }else if($i == 5){
            if($_data == NULL){
                $_data = NULL;
            }else{
                  $datetime = new DateTime($_data);
                  $my_timezone = new DateTimeZone($user_timezone);
                  $datetime->setTimezone($my_timezone);
                  $final_date = $datetime->format('dMY H:i:s');
                  $_data = $final_date;
            }
        }else if($i == 6){
            if($_data == NULL){
                $_data = NULL;
            }else{
                  $datetime = new DateTime($_data);
                  $my_timezone = new DateTimeZone($user_timezone);
                  $datetime->setTimezone($my_timezone);
                  $due_date = $datetime->format('dMY H:i:s');
                  $_data = $due_date;
            }

        }

        $row[] = $_data;
    }
    ob_start();
    $progress = ob_get_contents();
    ob_end_clean();
    $row[]              = $progress;
    $row['DT_RowClass'] = 'has-row-options';
    $output['aaData'][] = $row;
}
