<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Teams_model extends App_Model
{
    const STATUS_NOT_STARTED = 1;

    const STATUS_AWAITING_FEEDBACK = 2;

    const STATUS_TESTING = 3;

    const STATUS_IN_PROGRESS = 4;

    const STATUS_COMPLETE = 5;

    public function __construct()
    {
        parent::__construct();
    }

    public function get_statuses()
    {
        $statuses = hooks()->apply_filters('before_get_task_statuses', [
            [
                'id'             => self::STATUS_NOT_STARTED,
                'color'          => '#989898',
                'name'           => _l('task_status_1'),
                'order'          => 1,
                'filter_default' => false,
                ],
             [
                'id'             => self::STATUS_IN_PROGRESS,
                'color'          => '#03A9F4',
                'name'           => _l('task_status_4'),
                'order'          => 2,
                'filter_default' => true,
                ],
             [
                'id'             => self::STATUS_TESTING,
                'color'          => '#2d2d2d',
                'name'           => _l('task_status_3'),
                'order'          => 3,
                'filter_default' => false,
                ],
              [
                'id'             => self::STATUS_AWAITING_FEEDBACK,
                'color'          => '#adca65',
                'name'           => _l('task_status_2'),
                'order'          => 4,
                'filter_default' => false,
                ],
            [
                'id'             => self::STATUS_COMPLETE,
                'color'          => '#84c529',
                'name'           => _l('task_status_5'),
                'order'          => 100,
                'filter_default' => false,
                ],
            ]);

        usort($statuses, function ($a, $b) {
            return $a['order'] - $b['order'];
        });

        return $statuses;
    }

    public function get_teams(){
        $this->db->order_by('order_teams ASC');
        return $this->db->get(db_prefix().'teams')->result();
    }

    public function get_teams_summary($teamid){
        $this->db->select('*');
        $this->db->from(db_prefix().'staff');
        $this->db->join(db_prefix().'task_assigned',db_prefix().'staff.staffid = '.db_prefix().'task_assigned.staffid','left');
        $this->db->join(db_prefix().'tasks',db_prefix().'task_assigned.taskid = '.db_prefix().'tasks.id','left');
        $this->db->where(db_prefix().'staff.teamid',$teamid);
        $this->db->where('tbltasks.status != 5');
        $q = $this->db->get();
        return $q->result();        
    }

    public function get_team_users($teamid){
        $this->db->select('*');
        $this->db->from(db_prefix().'staff');
        $this->db->where(db_prefix().'staff.teamid',$teamid);
        return $this->db->get()->num_rows();
    }

    public function get_tasks(){
        return $this->db->get('tasks')->result();
    }

    public function get_gantt_tasks($teamid = '', $member = '', $dates = '', $status = ''){


        $monday = strtotime("last monday");
        $monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
        $sunday = strtotime(date("Y-m-d",$monday)." +6 days");
        $this_week_sd = date("Y-m-d",$monday);
        $this_week_ed = date("Y-m-d",$sunday);

        $this->db->select('*');
        $this->db->from(db_prefix().'tasks');
        $this->db->join(db_prefix().'task_assigned',db_prefix().'tasks.id = '.db_prefix().'task_assigned.taskid','left');
        $this->db->join(db_prefix().'staff',db_prefix().'task_assigned.staffid = '.db_prefix().'staff.staffid');
        if($teamid != null){
            $checkTeam = implode(', ', $teamid);
            if($checkTeam != "0"){
                $this->db->where(db_prefix().'staff.teamid IN ('. implode(', ', $teamid) .')');
            }
        }
        
        if($member != null ){
            $checkMember = implode(', ', $member);
            if($checkMember != "0"){
                $this->db->where(db_prefix().'task_assigned.staffid IN ('. implode(', ', $member) .')');
            }
        }

        if($dates != null){
            $daterange = explode('-', $dates);
            $this->db->where('startdate >= "'.date('Y-m-d', strtotime($daterange[0])).'" AND duedate <= "'.date('Y-m-d', strtotime($daterange[1])).'"');
        } 
        else {
            $this->db->where('startdate >= "'.date('Y-m-d', strtotime($this_week_sd)).'" AND duedate <= "'.date('Y-m-d', strtotime($this_week_ed)).'"');
        }
         
        if($status != null){
            $checkStatus = implode(', ', $status);
            if($checkStatus != "0"){
                $this->db->where(db_prefix().'tasks.status IN ('. implode(', ', $status) .')');
            }
        }

        $q = $this->db->get()->result_array();
        // $q->result_array();
        $gantt_data = [];

        foreach($q as $task){
            $data             = [];
            $data['values']   = [];
            $values           = [];

            $data['name'] = $task['firstname']." ".$task['lastname'];
            $data['desc'] = '';
            if($dates != null){
                $daterange = explode('-', $dates);
                $data['this_week_sd'] = date('Y-m-d 00:00:00', strtotime($daterange[0]));
                $data['this_week_ed'] = date('Y-m-d 23:59:59', strtotime($daterange[1]));
            } else {
                $data['this_week_sd'] = date('Y-m-d 00:00:00', strtotime($this_week_sd));
                $data['this_week_ed'] = date('Y-m-d 23:59:59', strtotime($this_week_ed));
            }
                
            $values['from']  =  $task['startdate'];
            $values['to']    =  $task['duedate'];
            $values['label'] =  $task['name'];
            $values['desc']  =  $task['description'];
            $values['customClass']   =  'gantt_'.$this->get_team_color($task['teamid']);

            $values['dataObj'] = [
                'task_id' => $task['taskid'],
            ];
            $data['values'][]      = $values;
            $gantt_data[]          = $data;
        }

        if(empty($gantt_data)){
            $data['name'] = '';
            $data['desc'] = '';
            if($dates != null){
                $daterange = explode('-', $dates);
                $data['this_week_sd'] = date('Y-m-d 00:00:00', strtotime($daterange[0]));
                $data['this_week_ed'] = date('Y-m-d 23:59:59', strtotime($daterange[1]));
            } else {
                $data['this_week_sd'] = date('Y-m-d 00:00:00', strtotime($this_week_sd));
                $data['this_week_ed'] = date('Y-m-d 23:59:59', strtotime($this_week_ed));
            }

            $values['from']  =  '';
            $values['to']    =  '';
            $values['label'] =  '';
            $values['desc']  =  '';
            $values['customClass']   =  '';

            $values['dataObj'] = [
                'task_id' => '',
            ];
            $data['values'][]      = $values;
            $gantt_data[]          = $data;
        }

        return $gantt_data;

    }

    public function get_team_color($teamid){
        if($teamid==1){
            return 'red';
        }else if($teamid==2){
            return 'blue';
        }else if($teamid==3){
            return 'green';
        }else if($teamid==4){
            return 'orange';
        }else if($teamid==5){
            return 'yellow';
        }else if($teamid==6){
            return 'brown';
        }else if($teamid==7){
            return 'purple';
        }else{
            return 'black';
        }
    }
    
}
