<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Changelog extends CI_Controller
{

    const STATUS = [
        1 => 'Open',
        2 => 'In progress',
        3 => 'Answered',
        4 => 'On Hold',
        5 => 'Closed'
    ];

    const SERVICES = [
        0 => 'Unassigned',
        1 => 'Integrations',
        2 => 'Product',
        3 => 'Issue Report',
        4 => 'Troubleshooting',
        5 => 'Knowledge Base Support',
        6 => 'Reporting',
        7 => 'Campaign Support'
    ];

    const CAMPAIGN_STATUS = [
        1 => 'Not Started',
        2 => 'In CS',
        3 => 'In Design',
        4 => 'Completed',
        5 => 'In QA',
        6 => 'With Client',
        7 => 'Delivered',
        8 => 'Live',
        9 => 'In Production',
        10 => 'On Hold',
    ];


    public function partners()
    {

        $this->db->select('company, datecreated, addedfrom');
        $this->db->from('tblclients');
        $this->db->order_by('userid', 'desc');
        $query=$this->db->get();


         $file_name = 'partner_created_date'.date('Ymd').'.csv'; 
         header("Content-Description: File Transfer"); 
         header("Content-Disposition: attachment; filename=$file_name"); 
         header("Content-Type: application/csv;");
       
         // file creation 
         $file = fopen('php://output', 'w');
     
         $header = array("Partner Name", "Created", "Created By"); 
         fputcsv($file, $header);
         foreach ($query->result_array() as $key => $value)
         { 

            $datecreated = date_create($value['datecreated']);
            $value['datecreated'] = date_format($datecreated,"m/d/Y g:i a");

            $value['addedfrom'] = $this->getName($value['addedfrom']);


           fputcsv($file, $value); 
         }
         fclose($file); 
         exit; 
    }




    public function campaigns()
    {

        $this->db->select('
            tblprojects.id as campaign_id, 
            tblprojects.status as camp_status, 
            tblprojects.name, 
            tblprojects.campaign_type, 
            tblprojects.channel, 
            tblprojects.start_date, 
            tblprojects.deadline, 
            tblprojects.project_created, 
            tblstaff.firstname, 
            tblstaff.lastname
        ');

        $this->db->from('tblprojects');
        $this->db->join('tblstaff', 'tblprojects.addedfrom = tblstaff.staffid');
        $this->db->order_by('tblprojects.id', 'desc');
        $query=$this->db->get();


         $file_name = 'campaigns'.date('Ymd').'.csv'; 
         header("Content-Description: File Transfer"); 
         header("Content-Disposition: attachment; filename=$file_name"); 
         header("Content-Type: application/csv;");
       
         // file creation 
         $file = fopen('php://output', 'w');
     
         $header = array(
            "Campaign ID",
            "Campaign Stage",
            "Campaign Name", 
            "Type", 
            "Channel", 
            "Launch", 
            "Delivery", 
            "Created", 
            "Created By"
        ); 
        fputcsv($file, $header);
        foreach ($query->result_array() as $key => $value){ 
            $value['camp_status'] = self::CAMPAIGN_STATUS[$value['camp_status']];
            $value['firstname'] = $value['firstname']." ".$value['lastname'];
            unset($value['lastname']);
            fputcsv($file, $value); 
        }
        fclose($file); 
        exit; 
    }



    public function campaigns_change()
    {

        $this->db->select('
            tblprojects.id as campaign_id, 
            tblprojects.name, 
            tbl_changelog_campaigns.data_type, 
            tbl_changelog_campaigns.from_value, 
            tbl_changelog_campaigns.to_value, 
            tbl_changelog_campaigns.dateupdated, 
            tbl_changelog_campaigns.staffid
        ');
        $this->db->from('tblprojects');
        $this->db->join('tbl_changelog_campaigns', 'tblprojects.id = tbl_changelog_campaigns.campaign_id');
        $this->db->order_by('tbl_changelog_campaigns.dateupdated', 'desc');
        $query=$this->db->get();


         $file_name = 'campaign_changes'.date('Ymd').'.csv'; 
         header("Content-Description: File Transfer"); 
         header("Content-Disposition: attachment; filename=$file_name"); 
         header("Content-Type: application/csv;");
       
         // file creation 
         $file = fopen('php://output', 'w');
     
         $header = array(
            "Campaign ID", 
            "Campaign Name", 
            "Type", 
            "Old Value", 
            "New Value", 
            "Updated", 
            "Updated By"
        ); 
         fputcsv($file, $header);
         foreach ($query->result_array() as $key => $value)
         { 

            if($value['data_type'] == 'Status'){
                $value['from_value'] = self::CAMPAIGN_STATUS[$value['from_value']];
                $value['to_value'] = self::CAMPAIGN_STATUS[$value['to_value']];
            }

           fputcsv($file, $value); 
        }
        fclose($file); 
        exit; 
    }




    public function tasks()
    {

        $user_ids = array('54','55','56','57','58','59');
        $this->db->select('
            tbltasks.id as task_id, 
            tblprojects.id as project_id, 
            tblprojects.status as camp_status, 
            tbltasks.name as taskname, 
            tbltask_types.task_type_name as task_type, 
            tbltasks.dateadded as dateadded, 
            tblprojects.name as campaignname, 
            tblclients.company as partner, 
            tblcustomfieldsvalues.value as channel, 
            tbltasks.startdate as startdate, 
            tbltasks.duedate as duedate, 
            tbltasks.datefinished as datefinished, 
            tbltask_assigned.staffid as assignedto, 
            tbltask_assigned.taskid as taskid, 
            tbltask_assigned.taskid as totaltime, 
            tblstaff.firstname as firstname, 
            tblstaff.lastname as lastname,
            tblteams.name as team_name,
            tbltasks.qa_result,
            tbltasks.qa_reasons
        ');
        $this->db->from('tbltasks');
        $this->db->join('tbltask_types', 'tbltasks.task_type = tbltask_types.task_type_id');
        $this->db->join('tblcustomfieldsvalues', 'tbltasks.id = tblcustomfieldsvalues.relid');
        $this->db->join('tblprojects', 'tbltasks.rel_id = tblprojects.id');
        $this->db->join('tblclients', 'tblprojects.clientid = tblclients.userid');
        $this->db->join('tblstaff', 'tbltasks.addedfrom = tblstaff.staffid');
        $this->db->join('tbltask_assigned', 'tbltask_assigned.taskid = tbltasks.id');
        $this->db->join('tblteams', 'tblteams.teamid = tbltasks.teamid');
        $this->db->where('tblcustomfieldsvalues.fieldto', 'tasks');
        $this->db->where('tblcustomfieldsvalues.fieldid', '3');
        $this->db->where_not_in('tbltask_assigned.staffid', $user_ids);
        $this->db->order_by('tbltasks.dateadded', 'desc');
        $query=$this->db->get();
        // echo "<pre>";
        // var_dump($query->result_array());
        // echo "</pre>";
         $file_name = 'tasks'.date('Ymd').'.csv'; 
         header("Content-Description: File Transfer"); 
         header("Content-Disposition: attachment; filename=$file_name"); 
         header("Content-Type: application/csv;");
       
         // file creation 
         $file = fopen('php://output', 'w');
     
         $header = array(
            "Task ID", 
            "Campaign ID",
            "Campaign Stage",
            "Task Name", 
            "Task Type", 
            "Created", 
            "Campaign", 
            "Partner", 
            "Channel", 
            "Start Date", 
            "Due Date", 
            "Resolved", 
            "Assigned to", 
            "Time Spent", 
            "Total Time Spent Combined", 
            "Created By",
            "Teams",
            "QA Result", 
            "QA Reasons"
        ); 
         fputcsv($file, $header);
         foreach ($query->result_array() as $key => $value)
         { 
            $value['firstname'] = $value['firstname']." ".$value['lastname'];
            unset($value['lastname']);

            $value['taskid'] = seconds_to_time_format($this->tasks_model->calc_task_total_time($value['taskid'],' AND staff_id='.$value['assignedto'])); // INDIVIDUAL TIME SPENT

            $value['totaltime'] = seconds_to_time_format($this->tasks_model->calc_task_total_time($value['totaltime'])); // TOTAL TIME SPENT
            $value['assignedto'] = $this->getName($value['assignedto']);
            $value['camp_status'] = self::CAMPAIGN_STATUS[$value['camp_status']];
            fputcsv($file, $value); 
         }
         fclose($file); 
         exit; 
    }




    public function tasks_reopened(){

        $this->db->select('
            tbltasks.id as task_id, 
            tblprojects.id as project_id, 
            tblprojects.status as camp_status, 
            tbltasks.name, 
            tbl_changelog_task_reopen.date_reopen, 
            tbl_changelog_task_reopen.reason, 
            tbl_changelog_task_reopen.other_reason, 
            tbl_changelog_task_reopen.comment, 
            tbl_changelog_task_reopen.staff,
            tbltasks.duedate as duedate,
            tblteams.name as team_name
        ');
        $this->db->from('tbltasks');
        $this->db->join('tbl_changelog_task_reopen', 'tbltasks.id = tbl_changelog_task_reopen.taskid');
        $this->db->join('tblprojects', 'tbltasks.rel_id = tblprojects.id');
        $this->db->join('tblteams', 'tblteams.teamid = tbltasks.teamid');
        $this->db->order_by('tbl_changelog_task_reopen.date_reopen', 'desc');
        $query=$this->db->get();
        // var_dump($query->result_array());

        $file_name = 'tasks_reopened'.date('Ymd').'.csv'; 
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$file_name"); 
        header("Content-Type: application/csv;");
    
        // file creation 
        $file = fopen('php://output', 'w');
    
        $header = array(
            "Task ID", 
            "Campaign ID",
            "Campaign Stage",
            "Task Name", 
            "Re-Opened", 
            "Reason", 
            "Other Reason", 
            "Notes", 
            "Re-Opened By",
            "Due Date", 
            "Teams",
        ); 

        fputcsv($file, $header);
        foreach ($query->result_array() as $key => $value){ 
            $value['camp_status'] = self::CAMPAIGN_STATUS[$value['camp_status']];
            fputcsv($file, $value); 
        }

        fclose($file); 
        exit; 
}



    public function tasks_change()
    {

        $this->db->select('
            tbltasks.id as tasks_id,
            tblprojects.id as project_id,
            tblprojects.status as camp_status,
            tbltasks.name,
            tbl_changelog_tasks.data_type, 
            tbl_changelog_tasks.from_value, 
            tbl_changelog_tasks.to_value, 
            tbl_changelog_tasks.dateupdated,
            tbltasks.duedate,
            tbl_changelog_tasks.staffid,
            tblteams.name as team_name,
        ');
        $this->db->from('tbltasks');
        $this->db->join('tbl_changelog_tasks', 'tbltasks.id = tbl_changelog_tasks.task_id');
        $this->db->join('tblprojects', 'tbltasks.rel_id = tblprojects.id');
        $this->db->join('tblteams', 'tblteams.teamid = tbltasks.teamid');
        $this->db->order_by('tbltasks.id', 'desc');
        $query=$this->db->get();


         $file_name = 'task_changes'.date('Ymd').'.csv'; 
         header("Content-Description: File Transfer"); 
         header("Content-Disposition: attachment; filename=$file_name"); 
         header("Content-Type: application/csv;");
       
         // file creation 
         $file = fopen('php://output', 'w');
     
         $header = array(
            "Task ID", 
            "Campaign ID",
            "Campaing Stage",
            "Task Name",
            "Type",  
            "Old Value", 
            "New Value", 
            "Updated",
            "Due Date",
            "Updated By",
            "Teams"
        ); 
         fputcsv($file, $header);
         foreach ($query->result_array() as $key => $value)
         { 

            if($value['data_type'] == 'Status'){
                if($value['from_value'] == 1):
                    $value['from_value'] = 'Not Started';
                elseif($value['from_value'] == 2):
                    $value['from_value'] = 'Awaiting Feedback';
                elseif($value['from_value'] == 3):
                    $value['from_value'] = 'Testing';
                elseif($value['from_value'] == 4):
                    $value['from_value'] = 'In Progress';
                elseif($value['from_value'] == 5): 
                    $value['from_value'] = 'Complete';
                endif;

                if($value['to_value'] == 1):
                    $value['to_value'] = 'Not Started';
                elseif($value['to_value'] == 2):
                    $value['to_value'] = 'Awaiting Feedback';
                elseif($value['to_value'] == 3):
                    $value['to_value'] = 'Testing';
                elseif($value['to_value'] == 4):
                    $value['to_value'] = 'In Progress';
                elseif($value['to_value'] == 5): 
                    $value['to_value'] = 'Complete';
                endif;
            }

            $value['camp_status'] = self::CAMPAIGN_STATUS[$value['camp_status']];
            fputcsv($file, $value); 
         }
         fclose($file); 
         exit; 
    }




     public function getName($id){
            $this->db->select('firstname, lastname');
            $this->db->from('tblstaff');
            $this->db->where('staffid', $id);
            $query=$this->db->get()->row();

            if($query){
                $name = $query->firstname." ".$query->lastname;
                return $name;
            }else{
                return 'Unknown';
            }
       }


      public function getDepartment($id){
            $this->db->select('name');
            $this->db->from('tbldepartments');
            $this->db->where('departmentid', $id);
            $query=$this->db->get()->row();
            $name = $query->name;
            return $name;
       }

      public function getService($id){
            $this->db->select('name');
            $this->db->from('tblservices');
            $this->db->where('serviceid', $id);
            $query=$this->db->get()->row();
            $name = $query->name;
            return $name;
       }

    public function tasks_assigned_time()
    {


        $this->db->select('
            tbltasks.id as tasks_id,
            tblprojects.id as project_id,
            tblprojects.status as camp_status,
            tbltasks.name, 
            tbl_changelog_task_assigned.dateadded, 
            tbl_changelog_task_assigned.staffid, 
            tbl_changelog_task_assigned.assigned_from,
            tbltasks.duedate,
            tblteams.name as team_name,
        ');
        $this->db->from('tbltasks');
        $this->db->join('tbl_changelog_task_assigned', 'tbltasks.id = tbl_changelog_task_assigned.taskid');
        $this->db->join('tblprojects', 'tbltasks.rel_id = tblprojects.id');
        $this->db->join('tblteams', 'tblteams.teamid = tbltasks.teamid');
        $this->db->order_by('tbltasks.id', 'desc');
        $query=$this->db->get();


         $file_name = 'task_assigned_time'.date('Ymd').'.csv'; 
         header("Content-Description: File Transfer"); 
         header("Content-Disposition: attachment; filename=$file_name"); 
         header("Content-Type: application/csv;");
       
         // file creation 
         $file = fopen('php://output', 'w');
     
         $header = array(
            "Task ID", 
            "Campaign ID",
            "Campaing Stage",
            "Task Name", 
            "Date Time Assigned",  
            "Assigned To", 
            "Assigned By",
            "Due Date",
            "Teams",
        ); 
         fputcsv($file, $header);
         foreach ($query->result_array() as $key => $value)
         { 
            $value['camp_status'] = self::CAMPAIGN_STATUS[$value['camp_status']];
            $value['staffid'] = $this->getName($value['staffid']);
            $value['assigned_from'] = $this->getName($value['assigned_from']);
               fputcsv($file, $value); 
         }
         fclose($file); 
         exit; 
    }


    public function tickets_change()
    {

        $this->db->select('tbltickets.subject, tbl_changelog_ticket.type, tbl_changelog_ticket.old_value, tbl_changelog_ticket.new_value, tbl_changelog_ticket.dateupdated, tbl_changelog_ticket.staffid');
        $this->db->from('tbltickets');
        $this->db->join('tbl_changelog_ticket', 'tbltickets.ticketid = tbl_changelog_ticket.ticket_id');
        $this->db->order_by('tbltickets.ticketid', 'desc');
        $query=$this->db->get();

         $file_name = 'ticket_changes'.date('Ymd').'.csv'; 
         header("Content-Description: File Transfer"); 
         header("Content-Disposition: attachment; filename=$file_name"); 
         header("Content-Type: application/csv;");
       
         // file creation 
         $file = fopen('php://output', 'w');	 
         $header = array("Ticket Subject", "Type", "Old Value", "New Value", "Updated", "Updated By"); 
         fputcsv($file, $header);
         foreach ($query->result_array() as $key => $value)
         { 

            if($value['type'] == 'status'){
                 if($value['old_value'] == 1):
                     $value['old_value'] = 'Open';
                 elseif($value['old_value'] == 2):
                     $value['old_value'] = 'In Progress';
                 elseif($value['old_value'] == 3):
                     $value['old_value'] = 'Answered';
                 elseif($value['old_value'] == 4):
                     $value['old_value'] = 'On Hold';
                 elseif($value['old_value'] == 5): 
                     $value['old_value'] = 'Closed';
                 endif;

                 if($value['new_value'] == 1):
                     $value['new_value'] = 'Open';
                 elseif($value['new_value'] == 2):
                     $value['new_value'] = 'In Progress';
                 elseif($value['new_value'] == 3):
                     $value['new_value'] = 'Answered';
                 elseif($value['new_value'] == 4):
                     $value['new_value'] = 'On Hold';
                 elseif($value['new_value'] == 5): 
                     $value['new_value'] = 'Closed';
                 endif;
             }
             elseif($value['type'] == 'department'){
                 $value['old_value'] = $this->getDepartment($value['old_value']);
                 $value['new_value'] = $this->getDepartment($value['new_value']);	     		
             }
             elseif($value['type'] == 'assigned'){
                 $value['old_value'] = $this->getName($value['old_value']);
                 $value['new_value'] = $this->getName($value['new_value']);
             }
             elseif($value['type'] == 'priority'){
                 if($value['old_value'] == 1):
                     $value['old_value'] = 'Low';
                 elseif($value['old_value'] == 2):
                     $value['old_value'] = 'Medium';
                 elseif($value['old_value'] == 3):
                     $value['old_value'] = 'High';
                 elseif($value['old_value'] == 4):
                     $value['old_value'] = 'Urgent';
                 endif;

                 if($value['new_value'] == 1):
                     $value['new_value'] = 'Low';
                 elseif($value['new_value'] == 2):
                     $value['new_value'] = 'Medium';
                 elseif($value['new_value'] == 3):
                     $value['new_value'] = 'High';
                 elseif($value['new_value'] == 4):
                     $value['new_value'] = 'Urgent';
                 endif;
             }
             elseif($value['type'] == 'service'){
                 if($value['old_value'] == 0):
                    $value['old_value'] = "None";
                else:
                    $value['old_value'] = $this->getService($value['old_value']);
                endif;
            

                if($value['new_value'] == 0):
                    $value['new_value'] = "None";
                else:
                    $value['new_value'] = $this->getService($value['new_value']);
                endif;
             }
             $value['type'] = ucfirst($value['type']);

           fputcsv($file, $value); 
         }
         fclose($file); 	
         exit; 
    }

    public function tickets_change_status()
    {
        $this->db->select('
            tbltickets.subject, 
            tbltickets.date, 
            tbldepartments.name as d_name, 
            tbltickets.service, 
            CASE
                WHEN tbltickets.assigned = 0 THEN ""
                ELSE (SELECT CONCAT(firstname, " ", lastname) FROM tblstaff WHERE staffid = tbltickets.assigned)
            END as assignee,
            tbltickets.name, 
            tbl_changelog_ticket.new_value, 
            tbl_changelog_ticket.dateupdated, 
            tbl_changelog_ticket.type
        ');
        $this->db->from('tbltickets');
        $this->db->join('tbl_changelog_ticket', 'tbltickets.ticketid = tbl_changelog_ticket.ticket_id');
        $this->db->join('tbldepartments', 'tbltickets.department = tbldepartments.departmentid');
        $this->db->order_by('tbltickets.ticketid', 'desc');
        $query=$this->db->get();

         $file_name = 'ticket_changes_status'.date('Ymd').'.csv'; 
         header("Content-Description: File Transfer"); 
         header("Content-Disposition: attachment; filename=$file_name"); 
         header("Content-Type: application/csv;");
       
         // file creation 
         $file = fopen('php://output', 'w');	 
         $header = array("Subject", "Date Created", "Department", "Service", "Assignee", "Contact", "Status", "Timestamp"); 
         fputcsv($file, $header);
         foreach ($query->result_array() as $key => $value)
         { 
            if($value['type'] == 'status'){
                $value['new_value'] = self::STATUS[$value['new_value']];
                $value['service'] = self::SERVICES[$value['service']];

                fputcsv($file, $value); 
            }
         }
         fclose($file); 	
         exit; 
    }


}
