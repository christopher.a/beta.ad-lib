<?php

    defined('BASEPATH') or exit('No direct script access allowed');
    
    class Api extends CI_Controller{
        
        public function __construct(){
            parent::__construct();
            $this->load->model('projects_model');
            $this->load->model('currencies_model');
            $this->load->helper('date');
        }
        
        public function parse_json(){
            $arr = array();
            $data = json_decode(file_get_contents('php://input'), true);
            print_r($data);
            $decoded = json_decode($data['data']);
            foreach($decoded as $row){
                $campaign = array(
                    'row' => $row->row,
                    'campaign_name' => $row->campaign_name,
                    'campaign_start_date' => $row->start_date,
                    'processed' => $row->processed,
                );
                if($this->db->get_where('tblprojects',array('row'=>$campaign['row']))->num_rows() == 1){
                    if($campaign['processed'] == "FALSE"){
                        $this->db->set('name', $campaign['campaign_name']);
                        $this->db->where('row',$campaign['row']);
                        $result = $this->db->update('tblprojects');
                    }
                    
                    if(($campaign['campaign_start_date'] != null) || ($campaign['campaign_start_date']) != ''){
                        $this->campaign_set_start_date($campaign);
                    }
                }else{
                    $this->create_campaign($campaign);
                    if($campaign['campaign_start_date'] == null || $campaign['campaign_start_date'] == ''){
                        $this->campaign_set_start_date($campaign);
                    }
                }
            }
            echo json_encode($arr);
        }
    
        public function campaign_set_start_date($data){
                $date = strtotime($data['campaign_start_date']);
                $this->db->set('start_date', date('Y-m-d', $date));
                $this->db->where('row',$data['row']);
                $this->db->update('tblprojects');
        }
        
        public function create_campaign($data){
            $result['success'] = false;
            //$data = json_decode(file_get_contents('php://input'), true);
            $result['campaign_name'] = $data['campaign_name'];
                    
                    $insert = array(
                        "name" => $data['campaign_name'],
                        "row" => $data['row'],
                        "clientid" => 4,
                        "progress_from_tasks" => 1,
                        "progress" => 0,
                        "billing_type" => 3,
                        "status" => 2,
                        "project_cost" => null,
                        "project_rate_per_hour" => null,
                        "estimated_hours" => null,
                        "start_date" => date('Y-m-d'),
                        "deadline" => null,
                        "original_delivery" => null,
                        "channel"   =>  "",
                        "io_id"     =>  0,
                        "platform_link" =>  '',
                        "description"   =>  ''
                    );
                    $result['row'] = $data['row'];
                    $this->db->insert('tblprojects',$insert);
                    $project_id = $this->db->insert_id();
                    $result['id'] = $project_id;
                    if ($project_id) {
                        
                        $project_settings = array(
                            'available_features' => 'a:15:{s:16:"project_overview";i:1;s:13:"project_tasks";i:1;s:18:"project_timesheets";i:1;s:18:"project_milestones";i:1;s:13:"project_files";i:1;s:19:"project_discussions";i:1;s:13:"project_gantt";i:1;s:15:"project_tickets";i:1;s:16:"project_invoices";i:1;s:17:"project_estimates";i:1;s:16:"project_expenses";i:1;s:20:"project_credit_notes";i:1;s:21:"project_subscriptions";i:1;s:13:"project_notes";i:1;s:16:"project_activity";i:1;}',
                            'view_tasks' => 1,
                            'create_tasks' => 1,
                            'edit_tasks' => 1,
                            'comment_on_tasks' => 1,
                            'view_task_comments' => 1,
                            'view_task_attachments' => 1,
                            'view_task_checklist_items' => 1,
                            'upload_on_tasks' => 1,
                            'view_task_total_logged_time' => 1,
                            'view_finance_overview' => 1,
                            'upload_files' => 1,
                            'open_discussions' => 1,
                            'view_milestones' => 1,
                            'view_gantt' => 1,
                            'view_timesheets' => 1,
                            'view_activity_log' => 1,
                            'view_team_members' => 1,
                            'hide_tasks_on_main_tasks_table' => 0
                        );
                        
                        foreach($project_settings as $key => $val){
                            $setting = array(
                                    "project_id" => $project_id,
                                    "name"  =>  $key,
                                    "value" =>  $val
                                );
                            $this->db->insert('tblproject_settings',$setting);
                        }
                        
                        $task = array(
                            'name' => 'Campaign Summary - '.$data['campaign_name'],
                            'startdate' => date("Y/m/d"),
                            'duedate' => date('Y-m-d', strtotime(date("Y-m-d"). ' + 2 days')),
                            'dateadded' => date("Y/m/d H:i:s"),
                            'rel_id' => $project_id,
                            'team' => 'CS (Client services)',
                            'status' => 1,
                            'priority' => 2,
                            'teamid' => 1,
                            'rel_type' => 'project'
                            );
                        $this->db->insert('tbltasks',$task);
                        $task_id = $this->db->insert_id();
                        
                        $items = array(
                            'Signals confirmed? (Max 2)',
                            'Strategy final?',
                            'DT structure confirmed?',
                            'Channels complete?',
                            'Formats and placements confirmed?',
                            'Delivery type final? (pre-rendered, trafficked, etc)',
                            'DSPs confirmed?',
                            'Brand Guidelines',
                            'Legal text needed?',
                            'Is Mobile QA needed? If yes, what sizes?',
                            'Success measurement established?',
                            'Do we need to accommodate for any special events/promos in the future?',
                            'Approval chain confirmed? ',
                            'Timelines agreed?',
                            'Assets reviewed and confirmed?',
                            'Tracking requirements confirmed?',
                            'Naming conventions'
                        );
                        for($x = 0; $x<count($items);$x++){
                            $checklist = array(
                                'taskid'    =>  $task_id,
                                'description'   =>  $items[$x],
                                'is_default'  =>  1,
                                'finished'  =>  0,
                                'dateadded' =>  date("Y-m-d H:i:s"),
                                'addedfrom' =>  1,
                                'finished_from' =>  0,
                                'list_order'    => $x+1
                            );
                            $this->db->insert('tbltask_checklist_items',$checklist);
                        }
                        
                        
                        $reminder = array(
                            'description' => 'auto-generated',
                            'date' => date('Y-m-d H:i:s', strtotime(date("Y-m-d H:i:s").'+ 2 days')),
                            'isnotified' => 0,
                            'rel_id' => $task_id,
                            'staff' => 24,
                            'rel_type' => 'task',
                            'notify_by_email' => 1,
                            'creator' => 24
                        );
                        $this->db->insert('tblreminders',$reminder);
                        
                        
                        $result['success'] = true;
                    }
            //echo json_encode($result);
        }
        
        
    }
