<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Todo extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('todo_model');
    }

    /* Get all staff todo items */
    public function index()
    {
        if ($this->input->is_ajax_request()) {
            echo json_encode($this->todo_model->get_todo_items($this->input->post('finished'), $this->input->post('todo_page'), $this->input->post('sort') ));
            exit;
        }
        $data['bodyclass']            = 'main-todo-page';
        $data['total_pages_finished'] = ceil(total_rows(db_prefix().'todos', [
            'finished' => 1,
            'staffid'  => get_staff_user_id(),
        ]) / $this->todo_model->getTodosLimit());
        $data['total_pages_unfinished'] = ceil(total_rows(db_prefix().'todos', [
            'finished' => 0,
            'staffid'  => get_staff_user_id(),
        ]) / $this->todo_model->getTodosLimit());
        $data['title'] = _l('my_todos');
        $this->load->view('admin/todos/all', $data);
    }

    public function public_lists($sort = 0)
    {
        $data['public_folder'] = $this->todo_model->get_public_folder();
        $data['todos'] = $this->todo_model->get_todo();
        $data['followed_folder'] = $this->todo_model->get_followed_folder();
        $data['public_folder_listed'] = $this->todo_model->get_public_folder_listed($sort);

        echo json_encode($data);
    }

    public function public_folder()
    {
        $data['public_folder'] = $this->todo_model->get_public_folder();
        $data['todos'] = $this->todo_model->get_todo();
        $data['followed_folder'] = $this->todo_model->get_followed_folder();

        $data['title'] = 'Public Folders';
        $this->load->view('admin/todos/public_folder', $data);
    }

    public function add_public_folder()
    {
        if ($this->input->post()) {
            $data = $this->input->post();
            if ($data['id'] == '') {
                $id = $this->todo_model->add_public_folder($data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('todo')));
                }
            } else {
                $id = $data['id'];
                unset($data['id']);
                $success = $this->todo_model->update_public_folder($id, $data);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('todo')));
                }
            }

            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    /* Add new todo item */
    public function todo()
    {
        if ($this->input->post()) {
            $data = $this->input->post();
            if ($data['todoid'] == '') {
                unset($data['todoid']);
                $id = $this->todo_model->add($data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('todo')));
                }
            } else {
                $id = $data['todoid'];
                unset($data['todoid']);
                $success = $this->todo_model->update($id, $data);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('todo')));
                }
            }

            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function get_by_id($id)
    {
        $todo              = $this->todo_model->get($id);
        $todo->description = clear_textarea_breaks($todo->description);
        echo json_encode($todo);
    }

    public function get_public_by_id($id)
    {
        $todo              = $this->todo_model->get_public($id);
        $todo->name = clear_textarea_breaks($todo->name);
        echo json_encode($todo);
    }

    /* Change todo status */
    public function change_todo_status($id, $status)
    {
        $success = $this->todo_model->change_todo_status($id, $status);
        if ($success) {
            set_alert('success', _l('todo_status_changed'));
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /* Update todo order / ajax */
    public function update_todo_items_order()
    {
        if ($this->input->post()) {
            $this->todo_model->update_todo_items_order($this->input->post());
        }
    }

    /* Delete todo item from databse */
    public function delete_todo_item($id)
    {
        if ($this->input->is_ajax_request()) {
            echo json_encode([
                'success' => $this->todo_model->delete_todo_item($id),
            ]);
        }
        die();
    }

    public function delete_public_folder($id)
    {
        if ($this->input->is_ajax_request()) {
            echo json_encode([
                'success' => $this->todo_model->delete_public_folder($id),
            ]);
        }
        die();
    }

    public function follow_public_folder($id)
    {
        if ($this->input->is_ajax_request()) {
            echo json_encode([
                'success' => $this->todo_model->follow_public_folder($id),
            ]);
        }
        die();
    }

    public function unfollow_public_folder($id)
    {
        if ($this->input->is_ajax_request()) {
            echo json_encode([
                'success' => $this->todo_model->unfollow_public_folder($id),
            ]);
        }
        die();
    }

    public function assign_to_me($id)
    {
        if ($this->input->is_ajax_request()) {
            echo json_encode([
                'success' => $this->todo_model->assign_to_me($id),
            ]);
        }
        die();
    }

    public function unassign_to_me($id)
    {
        if ($this->input->is_ajax_request()) {
            echo json_encode([
                'success' => $this->todo_model->unassign_to_me($id),
            ]);
        }
        die();
    }

    public function delete_completed()
    {
        echo json_encode([
            'success' => $this->todo_model->delete_completed($id),
        ]);
    }
}
