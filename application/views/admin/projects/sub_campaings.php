<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<!-- sub campaigns Tasks -->
<div class="clearfix"></div>
<div class="_buttons">
    <div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data" data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">

        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-filter" aria-hidden="true"></i>
        </button>
        <ul class="dropdown-menu dropdown-menu-right width300">
            <li>
            <a href="#" data-cview="all" id="all_projects" onclick="dt_custom_view('','.table-projects',''); return false;">
                <?php echo _l('expenses_list_all'); ?>
            </a>
            </li>
            <?php
            // Only show this filter if user has permission for projects view otherwise wont need this becuase by default this filter will be applied
            if(has_permission('projects','','view')){ ?>
            <li>
            <a href="#" id="my_projects" data-cview="my_projects" onclick="dt_custom_view('my_projects','.table-projects','my_projects'); return false;">
                <?php echo _l('home_my_projects'); ?>
            </a>
            </li>
            <?php } ?>
            <li class="divider"></li>
            <?php foreach($statuses as $status){ ?>
            <li class="<?php if($this->input->get('status') == $status['id']) {echo 'active';} ?>">
                <a href="#" data-cview="<?php echo 'project_status_'.$status['id']; ?>" onclick="dt_custom_view('project_status_<?php echo $status['id']; ?>','.table-projects','project_status_<?php echo $status['id']; ?>'); return false;">
                <?php echo $status['name']; ?>
                </a>
            </li>
            <?php } ?>
            </ul>
    </div>
</div>


<div class="row mbot15">
    <div class="col-md-12">
        <h4 class="no-margin">Sub-campaigns Summary</h4>
        <div class="_filters _hidden_inputs">
            <?php
            echo form_hidden('my_projects');
            foreach($statuses as $status){
            $value = $status['id'];
                if($status['filter_default'] == false && !$this->input->get('status')){
                $value = '';
                } else if($this->input->get('status')) {
                $value = ($this->input->get('status') == $status['id'] ? $status['id'] : "");
                }
                echo form_hidden('project_status_'.$status['id'],$value);
            ?>
            <div class="col-md-2 col-xs-6 border-right">
            <?php $where = 'status = '.$status['id']. ' AND parent_campaign = '.$project->id; ?>
            
            <a href="#" onclick="dt_custom_view('project_status_<?php echo $status['id']; ?>','.table-projects','project_status_<?php echo $status['id']; ?>',true); return false;">
                <h3 class="bold"><?php echo total_rows(db_prefix().'projects',$where); ?></h3>
                <span style="color:<?php echo $status['color']; ?>" project-status-<?php echo $status['id']; ?>>
                <?php echo $status['name']; ?>
                </span>
            </a>
            </div>
            <?php } ?>
        </div>
    </div>
</div>


<div class="clearfix"></div>

<div class="tasks-table">
    <?php 
        $table_data = [
            _l('the_number_sign'),
            _l('project_name'),
            'Channels',
            _l('tags'),
            _l('project_start_date'),
            _l('project_deadline'),
            _l('project_members'),
            _l('project_status'),
         ];
         
         $custom_fields = get_custom_fields('projects', ['show_on_table' => 1]);
         foreach ($custom_fields as $field) {
             array_push($table_data, $field['name']);
         }
         
         $table_data = hooks()->apply_filters('projects_table_columns', $table_data);
         
         render_datatable($table_data, isset($class) ?  $class : 'projects', [], [
           'data-last-order-identifier' => 'projects',
           'data-default-order'  => get_table_last_order('projects'),
         ]);
         
    ?>
</div>

<?php init_tail(); ?>

<script type="text/javascript">
$(function(){
     var ProjectsServerParams = {};
     var project_id = '<?php echo $project->id?>';

     $.each($('._hidden_inputs._filters input'),function(){
         ProjectsServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
     });

     initDataTable('.table-projects', admin_url+'projects/table_sub_campaing/'+project_id, undefined, undefined, ProjectsServerParams, <?php echo hooks()->apply_filters('projects_table_default_order', json_encode(array(5,'asc'))); ?>);

     init_ajax_search('customer', '#clientid_copy_project.ajax-search');
});
</script>

