<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>

<style type="text/css">
.accordion-toggle:before {
    font-family: 'Glyphicons Halflings';
    content: "\e114";
    color: grey;
    margin-right: 10px;
}
.accordion-toggle.collapsed:before {
    content: "\e080";
}

</style>

<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
            <div class="panel_s">
              <div class="panel-body">
              <div class="_buttons">
              <?php if(has_permission('projects','','create')){ ?>
                <a href="<?php echo admin_url('projects/project'); ?>" class="btn btn-info pull-left display-block mright5">
                  <?php echo _l('new_project'); ?>
                </a>
              <?php } ?>
              <a href="<?php echo admin_url('projects/gantt'); ?>" data-toggle="tooltip" title="<?php echo _l('project_gant'); ?>" class="btn btn-default"><i class="fa fa-align-left" aria-hidden="true"></i></a>
              <div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data" data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-filter" aria-hidden="true"></i>
                </button>
                <ul class="dropdown-menu dropdown-menu-right width300">
                  <li>
                    <a href="#" data-cview="all" id="all_projects" onclick="dt_custom_view('','.table-projects',''); return false;">
                      <?php echo _l('expenses_list_all'); ?>
                    </a>
                  </li>
                  <?php
                  // Only show this filter if user has permission for projects view otherwise wont need this becuase by default this filter will be applied
                  if(has_permission('projects','','view')){ ?>
                  <li>
                    <a href="#" id="my_projects" data-cview="my_projects" onclick="dt_custom_view('my_projects','.table-projects','my_projects'); return false;">
                      <?php echo _l('home_my_projects'); ?>
                    </a>
                  </li>
                  <?php } ?>
                  <li class="divider"></li>
                  <?php foreach($statuses as $status){ ?>
                    <li class="<?php if( $adminId == 0 &&  ($status['filter_default'] == true && !$this->input->get('status') || $this->input->get('status') == $status['id']) ){echo 'active';} ?>">
                      <a href="#" data-cview="<?php echo 'project_status_'.$status['id']; ?>" onclick="dt_custom_view('project_status_<?php echo $status['id']; ?>','.table-projects','project_status_<?php echo $status['id']; ?>'); return false;">
                        <?php echo $status['name']; ?>
                      </a>
                    </li>
                    <?php } ?>
                  </ul>
                </div>
                <div class="clearfix"></div>
                <hr class="hr-panel-heading" />
              </div>
               <div class="row mbot15">
                <div class="col-md-12">
                  <h4 class="no-margin"><?php echo _l('projects_summary'); ?></h4>
                  <?php
                  $_where = '';
                  if(!has_permission('projects','','view')){
                    $_where = 'id IN (SELECT project_id FROM '.db_prefix().'project_members WHERE staff_id='.get_staff_user_id().')';
                  }
                  ?>
                </div>
                <div class="_filters _hidden_inputs">
                  <?php
                  echo form_hidden('my_projects');
                  foreach($statuses as $status){
                   $value = $status['id'];
                     if($status['filter_default'] == false && !$this->input->get('status')){
                        $value = '';
                     } else if($this->input->get('status')) {
                        $value = ($this->input->get('status') == $status['id'] ? $status['id'] : "");
                     }
                     echo form_hidden('project_status_'.$status['id'],$value);
                    ?>
                   <div class="col-md-2 col-xs-6 border-right">
                    <?php $where = ($_where == '' ? '' : $_where.' AND ').'status = '.$status['id']; ?>
                    <a href="#" onclick="dt_custom_view('project_status_<?php echo $status['id']; ?>','.table-projects','project_status_<?php echo $status['id']; ?>',true); return false;">
                     <h3 class="bold"><?php echo total_rows(db_prefix().'projects',$where); ?></h3>
                     <span style="color:<?php echo $status['color']; ?>" project-status-<?php echo $status['id']; ?>>
                     <?php echo $status['name']; ?>
                     </span>
                   </a>
                 </div>
                 <?php } ?>
               </div>
             </div>
             <div class="clearfix"></div>
              <hr class="hr-panel-heading" />
             <?php echo form_hidden('custom_view'); ?>
             <a href="#" data-toggle="modal" data-target="#projects_bulk_actions" class="hide bulk-actions-btn table-btn" data-table=".table-projects"><?php echo _l('bulk_actions'); ?></a>
             <?php $this->load->view('admin/projects/table_html',array('bulk_actions'=>true)); ?>
             <?php $this->load->view('admin/projects/_bulk_actions'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/projects/copy_settings'); ?>
<?php init_tail(); ?>

<script type="text/javascript">
    var adminId = "<?php echo $adminId ?>"
    if(adminId == 1){
      setTimeout(function() {
        document.getElementById('all_projects').onclick();
      }, 1000);
    } else {
      setTimeout(function() {
        document.getElementById('my_projects').onclick();
      }, 1000);
    }
    setInterval(function () {
      $(".btn-dt-reload").click();
    }, 50000);

    $('.table-projects tbody').on('click', 'tr td:nth-child(3)', function(event) {
      var id = $(this).find('div.row-options').attr('data-id');
      var child = $(this).find('div.row-options').attr('data-child');
      const cars = [
        '', 
        '<li style="margin-bottom: 3px;"><span class="label label inline-block" style="color: #989898; border:1px solid #989898">Not Started</span></li>',
        '<li style="margin-bottom: 3px;"><span class="label label inline-block" style="color: #03a9f4; border:1px solid #03a9f4">In CS</span></li>',
        '<li style="margin-bottom: 3px;"><span class="label label inline-block" style="color: #ff6f00; border:1px solid #ff6f00">In Design</span></li>',
        '<li style="margin-bottom: 3px;"><span class="label label inline-block" style="color: #84c529; border:1px solid #84c529">In </span></li>',
        '<li style="margin-bottom: 3px;"><span class="label label inline-block" style="color: #899857; border:1px solid #899857">In QA</span></li>',
        '<li style="margin-bottom: 3px;"><span class="label label inline-block" style="color: #B72974; border:1px solid #B72974">With Client</span></li>',
        '<li style="margin-bottom: 3px;"><span class="label label inline-block" style="color: #8a6d3b; border:1px solid #8a6d3b">Delivered</span></li>',
        '<li style="margin-bottom: 3px;"><span class="label label inline-block" style="color: #381460; border:1px solid #381460">Live</span></li>',
        '<li style="margin-bottom: 3px;"><span class="label label inline-block" style="color: #ffe75e; border:1px solid #ffe75e">In Production</span></li>',
        '<li style="margin-bottom: 3px;"><span class="label label inline-block" style="color: #ffe75e; border:1px solid #ffe75e">On Hold</span></li>',
      ];

      if(child == 1){
        $.post(admin_url + 'projects/get_sub_campaigns/' + id).done(function(response) {
            response = JSON.parse(response);
            var html = '';
            var start_date = '';
            var end_date = '';
            var status = '';
            var partner = '';
            var member = '';
            var no_img = "<?php echo base_url('assets/images/user-placeholder.jpg'); ?>";
            var with_img = "<?php echo base_url('uploads/staff_profile_images'); ?>";

            html += '<br>';
            html += '<span><b>Sub Campaign</b></span>';
            html += '<ul>';

            start_date += '<br>';
            start_date += '<span><b></b></span>';
            start_date += '<ul>';

            end_date += '<br>';
            end_date += '<span><b></b></span>';
            end_date += '<ul>';

            status += '<br>';
            status += '<span><b></b></span>';
            status += '<ul>';

            partner += '<br>';
            partner += '<span><b></b></span>';
            partner += '<ul>';

            member += '<br>';
            member += '<span><b></b></span>';
            member += '<ul>';

            $.each(response['success'], function( index, value ) {
              html += '<li><a href="projects/view/'+value['id']+'" > '+value['name']+'</a></li>'

              start_date += '<li>'+value['start_date']+'</li>';

              end_date += '<li>'+value['deadline']+'</li>';

              status += cars[value['status']];

              partner += '<li><a href="clients/client/' + value['company_id'] +'" target="_blank">'+value['company']+'</a></li>';
              var names = value['members'];
              var nameArr = names.split(',');

              member += '<li>';
              $.each(nameArr, function( index, value ) {
                var img = value.split('-');
                
                if(img[1] == 00){
                  member += '<img src="'+no_img+'" class="staff-profile-image-small mright5">';
                } else {
                  member += '<img src="'+with_img+'/'+img[0]+'/small_'+img[1]+'" class="staff-profile-image-small mright5">';
                }
                
              });
                
              member += '</li>';
              
            });
            html += '</ul>';
            start_date += '</ul>';
            end_date += '</ul>';
            status += '</ul>';
            partner += '</ul>';
            member += '</ul>';

            $("#subCam"+id).html(html);
            $("#start_date"+id).html(start_date);
            $("#end_date"+id).html(end_date);
            $("#sub_status"+id).html(status);
            $("#partner"+id).html(partner);
            $("#member"+id).html(member);
            
        });
      }

    });

    function duedate($id){
        $('.project-list-createdate').datetimepicker({});
    }

</script>
<script>
$(function(){
     var ProjectsServerParams = {};

     $.each($('._hidden_inputs._filters input'),function(){
         ProjectsServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
     });

     initDataTable('.table-projects', admin_url+'projects/table', [0], [0], ProjectsServerParams, <?php echo hooks()->apply_filters('projects_table_default_order', json_encode(array(5,'asc'))); ?>);

     init_ajax_search('customer', '#clientid_copy_project.ajax-search');
});
</script>
</body>
</html>
