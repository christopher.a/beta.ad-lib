<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade bulk_actions" id="projects_bulk_actions" tabindex="-1" role="dialog" data-table="<?php echo (isset($table) ? $table : '.table-projects'); ?>">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo _l('bulk_actions'); ?></h4>
         </div>
         <div class="modal-body">
   
            <div id="bulk_change">
               <div class="form-group">
                  <label for="move_to_status_tasks_bulk_action"><?php echo _l('task_status'); ?></label>
                  <select name="move_to_status_tasks_bulk_action" id="move_to_status_tasks_bulk_action" data-width="100%" class="selectpicker" data-none-selected-text="<?php echo _l('task_status'); ?>">
                     <option value=""></option>
                     <?php foreach($statuses as $status){ ?>
                        <option value="<?php echo $status['id']; ?>"><?php echo $status['name']; ?></option>
                     <?php } ?>
                  </select>
               </div>

               <?php
                  echo '<i class="fa fa-question-circle" data-toggle="tooltip" data-title="'._l('tasks_bull_actions_assign_notice').'"></i>';
                  $staff_bulk_assigned = $this->staff_model->get('', ['active'=>1]);
                  echo render_select('task_bulk_assignees',$staff_bulk_assigned,array('staffid',array('firstname','lastname')),'task_assigned','',array('multiple'=>true));
                  if(isset($project)){
                    echo render_select('task_bulk_milestone', $this->projects_model->get_milestones($project->id), array(
                      'id',
                      'name'
                   ), 'task_milestone');
                } ?>
                <div class="form-group">
                  <?php echo '<p><b><i class="fa fa-tag" aria-hidden="true"></i> ' . _l('tags') . ':</b></p>'; ?>
                  <input type="text" class="tagsinput" id="tags_bulk" name="tags_bulk" value="" data-role="tagsinput">
               </div>
               
         </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
         <a href="#" class="btn btn-info" onclick="projects_bulk_action(this); return false;"><?php echo _l('confirm'); ?></a>
      </div>
   </div>
   <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
