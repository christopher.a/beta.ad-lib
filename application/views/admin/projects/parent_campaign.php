<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="_task_modal" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
   <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title" id="myModalLabel">
            Add new Parent Campaign
         </h4>
      </div>
      <div class="modal-body">
         <div class="row">
            <div class="col-md-12">
               <div class="form-group chk"><br>
                  <label class="control-label no-mbot">Parent Campaign Name</label>
                  <input type="hidden" id="url" value="<?php echo base_url(); ?>">
                  <input type="text" name="campaign_name" id="campaign_name" style="width: 100%;">
                </div>
            </div>
         </div>
      </div>


      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
         <button id="submit" class="btn btn-info">Submit</button>
      </div>
   </div>
</div>
<?php echo form_close(); ?>


<script>
$(document).ready(function() {
    //set initial state.
      $('#submit').click(function(){

            var baseurl       = $('#url').val();
            var campaign_name = $('#campaign_name').val();
            var info = {
              campaign_name : campaign_name
            };
            if(campaign_name == ""){
               alert_float('danger', 'Please enter parent campaign name');
            }else{
                $.ajax({
                  type: "POST",
                  dataType: "json",
                  url: baseurl + 'admin/projects/addParentCampaign',
                  data: info,
                  success: function(response){
                     if(response.success == true){
                        $('#_task_modal').modal('toggle');
                        alert_float('success', 'Parent Campaign was successfully added');
                     }
                  },        
                });
            }


      });

   });
</script>