<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>

<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="table-responsive">
                        <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Action</th>
                                        <th>Staff</th>
                                        <th>Old Values</th>
                                        <th>New Values</th>
                                        <th>Date Updated</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $logs_found = false;
                                        if(count($task_change_logs) > 0){
                                        $logs_found = true;
                                        }
                                    ?>
                                    <?php if($logs_found == false){ ?>
                                    <tr>
                                        <td colspan="5" class="text-center bold"><?php echo _l('no_timers_found'); ?></td>
                                    </tr>
                                    <?php } else { ?>
                                        <?php 
                                        foreach($task_change_logs as $logs) {
                                        ?>
                                        <tr>
                                            <td><?php echo $logs['data_type']; ?></td>
                                            <td><?php echo $logs['staffid']; ?></td>
                                            <td>
                                                <?php 
                                                    if($logs['data_type'] == 'Status'){
                                                    echo _l('task_status_'.$logs['from_value']);
                                                    } else if ($logs['data_type'] == 'Priority'){
                                                    echo _l('task_priority_'.$logs['from_value']);
                                                    } else {
                                                    echo $logs['from_value']; 
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                                <?php 
                                                    if($logs['data_type'] == 'Status'){
                                                    echo _l('task_status_'.$logs['to_value']);
                                                    } else if ($logs['data_type'] == 'Priority'){
                                                    echo _l('task_priority_'.$logs['to_value']);
                                                    } else {
                                                    echo $logs['to_value']; 
                                                    }
                                                ?>
                                            </td>
                                            <td><?php echo $logs['dateupdated']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>