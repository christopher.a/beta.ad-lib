<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="_task_modal" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
   <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title" id="myModalLabel">
            Re-Open Task - <?php echo $name; ?>
         </h4>
      </div>
      <div class="modal-body">
         <div class="row">
            <div class="col-md-12">
               <div class="form-group chk"><br>
                  <label class="control-label no-mbot">Reason/s</label>
                  <div class="checkbox">
                     <input type="hidden" id="url" value="<?php echo base_url(); ?>">
                     <input type="hidden" id="id" value="<?php echo $id; ?>">
                     <input type="hidden" id="status" value="<?php echo $status; ?>">
                     <input class="custom_field_checkbox" value="Internal-QA" id="internal_qa" type="checkbox" name="reasons">
                     <label for="internal_qa" class="cf-chk-label">Internal - QA</label>
                     <br>
                     <input class="custom_field_checkbox" value="Internal-Revision" id="internal_revision" type="checkbox" name="reasons">
                     <label for="internal_revision" class="cf-chk-label">Internal - Revision</label>
                     <br>

                     <input class="custom_field_checkbox"  value="Internal-Additional Request" id="internal_additionalrequest" type="checkbox" name="reasons">
                     <label for="internal_additionalrequest" class="cf-chk-label">Internal - Additional Request</label>
                     <br>

                     <input class="custom_field_checkbox" value="External-QA" id="external_qa" type="checkbox" name="reasons">
                     <label for="external_qa" class="cf-chk-label">External - QA</label>
                     <br>

                     <input class="custom_field_checkbox" value="External-Revision" id="external_revision" type="checkbox" name="reasons">
                     <label for="external_revision" class="cf-chk-label">External - Revision</label>
                     <br>

                     <input class="custom_field_checkbox"  value="External-Additional Request" id="external_additionalrequest" type="checkbox" name="reasons">
                     <label for="external_additionalrequest" class="cf-chk-label">External - Additional Request</label>
                     <br>

                     <input class="custom_field_checkbox" id="others" value="Others" type="checkbox" name="reasons">
                     <label for="others" class="cf-chk-label">Others</label>
                     <br>
                    
                     <input type="text" style="width: 90%; display:none;" id="others_description" name="others_description">
                  </div>



            </div>
         </div>
      </div>

      <div class="row">
         <div class="col-md-12">
          <hr>
          <label for="comments" class="cf-chk-label">Comments</label>
          <br>
        <textarea name="comments" id="comments" row="5" style="width: 100%;"></textarea>
        </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
         <button id="submit" class="btn btn-info">Submit</button>
      </div>
   </div>
</div>
<?php echo form_close(); ?>


<script>
$(document).ready(function() {
    //set initial state.
    $('#others').change(function() {
        if(this.checked) {
            $('#others_description').show();
        } else{
            $('#others_description').hide();
        }
    });

$('#submit').click(function(){

      if($('#others').prop("checked") == true){
         var others_description = $('#others_description').val();
         if(others_description == ""){
            alert_float('danger', 'Please input other reason');
         }
      }else{
         var others_description = '';
      }


      var baseurl       = $('#url').val();
      var id            = $('#id').val();
      var status        = $('#status').val();
      var comments      = $('#comments').val();

       var reasons = [];
         $.each($("input[name='reasons']:checked"), function(){
            reasons.push($(this).val());
      });
      reasons = reasons.join(",");
      var info    = {
         id: id,
         status: status,
         reasons: reasons,
         others_description: others_description,
         comments: comments
      };

      if(reasons == ""){
         alert_float('danger', 'Please select a reason/s');
      }else{
          $.ajax({
            type: "POST",
            dataType: "json",
            url: baseurl + 'admin/tasks/reopen_task_method',
            data: info,
            success: function(response){
               if(response.success == true){
                  $('#_task_modal').modal('toggle');
                  alert_float('success', 'Task was successfully Re-Opened');
                  reload_tasks_tables();
                  init_task_modal(id);
               }
            },        
          });
      }


});

  // $('#submit').validate({
  //   ignore: [],
  //   errorElement: 'span',
  //   errorClass: 'help-block',
  //   focusInvalid: true,

  //   highlight: function (element) {
  //     $(element).closest('.form-group').addClass('has-danger');
  //   },

  //   unhighlight: function (element) {
  //     $(element).closest('.form-group').removeClass('has-danger');
  //   },

  //   success: function (label) {
  //     label.closest('.form-group').removeClass('has-danger');
  //     label.remove();
  //   },

  //   errorPlacement: function (error, element) {
  //     if (element.parent('.input-group').length) {
  //       error.insertAfter(element.parent());
  //     } else {
  //       error.insertAfter(element);
  //     }
  //   },

  //   submitHandler: function (form) {
  //     var collectdata   = new FormData($('#reason')[0]);    
  //     var baseurl       = $('#url').val();
  //     var id            = $('#id').val();
  //     var status        = $('#status').val();
  //     var reasons       = $('#reasons').val();
  //     var info    = {
  //        id: id,
  //        status: status
  //     };
  //      $.ajax({
  //        type: "POST",
  //        dataType: "json",
  //        url: baseurl + 'execute-ci-findings',
  //        data: info,
  //        success: function(response){
  //          alert_float('success', 'Task was successfully Re-Opened');
  //        },        
  //      });
  //     }
  //   });
   });
</script>