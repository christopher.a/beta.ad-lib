<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<style type="text/css">
    .personal-header{
        font-size: 15px; 
        margin-left:5px;
        color: #ff6f00 !important;
    }

    .personal-header i{
        font-size: 20px;
        margin-right: 5px;
    }

    .btn-personal-header{
        font-size: 15px; 
        margin: 10px 15px;
        text-align: right;
        color: #ff6f00 !important;
    }

    .btn-personal-header i{
        font-size: 20px;
    }

    .sort-todo{
        float: right;
        cursor: pointer;
        width: 40px;
    }

    .sort-list{
        float: right;
        cursor: pointer;
        width: 40px;
    }

    .sort-todo:hover, .sort-list:hover, .collapse-list:hover, .expand-list:hover{
        background: rgba(54, 25, 25, .7);
    }

    .view-todo:hover, .hide-todo:hover{
        background: rgba(54, 25, 25, .5);
    }

    .personal:hover{
        background: rgba(255, 111, 0, .5);
    }

    .followed:hover{
        background: rgba(3, 169, 244, .5);
    }

    .view-todo, .hide-todo{
        float: right;
        margin-right: 20px;
        cursor: pointer;
        width: 35px;
        text-align: center;
    }

    .collapse-list, .expand-list{
        float: right;
        margin-right: 25px;
        cursor: pointer;
        width: 35px;
        text-align: center;
    }
    

    .active{
        background: rgba(54, 25, 25, .5);
    }

    .todo-title {
        font-size: 16px;
    }

    .main_headers{
        font-weight: bolder;
        color: grey;
    }

    .switch{
        width: 16%;
        position: relative;
        color: #fff;
        margin-bottom: 3%;
    }

    .personal{
        width: 50%;
        position: relative;
        float: left;
        background-color: #ff6f00;
        text-align: center;
        padding: 3px;
        border-top-left-radius: 7px;
        border-bottom-left-radius: 7px;
        cursor: pointer;
    }

    .followed{
        width: 50%;
        position: relative;
        float: left;
        background-color: #03a9f4;
        text-align: center;
        padding: 3px;
        border-top-right-radius: 7px;
        border-bottom-right-radius: 7px;
        cursor: pointer;
    }

    .view_public{
        float: right;
        font-size: 16px;
        font-weight: 800;
        margin-right: 5px;
    }

    .view_public{
        float: right;
        font-size: 16px;
        font-weight: 800;
        margin-right: 5px;
    }
    .public-folder{
        background-color: #C2E9FC;
        margin: 0px !important;
        padding: 10px 20px;
        color: #0FA9F4;
    }
    .btn-public-folder{
        float: right;
        font-size: 16px;
        font-weight: 800;
        margin-right: 5px;
    }

    .btn-public-folder-header{
        float: right;
        font-size: 16px;
        font-weight: 800;
        margin-right: 30px;
    }

    .follow{
        border: 1px solid;
        float: right;
        width: 90px;
        text-align: center;
        font-size: 13px;
        font-weight: 100;
        background-color: #fff;
        border-radius: 10px;
        padding: 2px 0px;
        margin-left: 5px;
    }
    .public_action{
        margin-right: 10px;
        font-size: 22px;
    }
    .public-tasks{
        background-color: #fff;
        margin-left: -20px;
        margin-right: -20px;
        color: gray;
    }

    .public-tasks li{
        padding: 5px 20px;
        font-size: 15px;
    }

    .public_add_tasks i{
        color: #03a9f4;
    }

    .public_add_tasks{
        margin-left: 20px;
    }

    .add-public-folder{
        float: right;
        font-size: 22px;
        font-weight: 800;
        margin-right: 25px;
    }

    #unfollow{
        background-color: #B31756;
        color: #fff;
        border-color: #B31756;
    }

    #delete_add{
        cursor: pointer;
    }

    .test:before {
        font-family: 'Glyphicons Halflings';
        content: "\e114";
        color: #008ece;
        font-size: 18px;
        right: 35px;
        position: absolute;
        top: 12px;
    }
    .test.collapsed:before {
        content: "\e080";
    }

</style>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="clearfix"></div>
                        <div class="switch">
                            <div class="personal">Personal</div>
                            <div class="followed">Followed</div>
                        </div>
                        <div class="_buttons">
                            <a href="<?php echo admin_url('todo/public_folder'); ?>" class="view_public">View all Public lists</a>
                            <h3 class="main_headers"> My To-do's </h3>
                            
                        </div>
                        
                        <hr class="hr-panel-heading" />
                        <div class="row">
                            <div class="col-md-6" id="personal-main">
                                <div class="panel_s events animated fadeIn">
                                    <div class="panel-body todo-body">
                                        <h4 class="todo-title warning-bg">
                                            Personal to-do's

                                            <span>Completed</span>
                                            <span class="view-todo">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </span>
                                            <span class="hide-todo" style="display: none;">
                                                <i class="fa fa-eye-slash" aria-hidden="true"></i>
                                            </span>
                                            <div class="sort-todo">
                                                <i class="fa fa-long-arrow-up" aria-hidden="true" style="margin-left: 10px;"></i>
                                                <i class="fa fa-long-arrow-down" aria-hidden="true"></i>
                                            </div>
                                            
                                        </h4>

                                            <div class="col-md-6">
                                                <h4 class="personal-header">
                                                    <a href="#__todo" data-toggle="modal" style="color: #ff6f00 !important">
                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                        <span>Add to-do</span>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div class="col-md-6">
                                                <h4 class="btn-personal-header" >
                                                    <span id="delete_add" >Delete all completed <i class="fa fa-trash-o" aria-hidden="true"></i></span>
                                                </h4>
                                            </div>

                                            <div class="col-md-12">
                                            <ul class="list-unstyled todo unfinished-todos todos-sortable">
                                                <li class="padding no-todos hide ui-state-disabled">
                                                    <?php echo _l('no_unfinished_todos_found'); ?>
                                                </li>
                                            </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center padding">
                                            <a href="" class="btn btn-default text-center unfinished-loader"><?php echo _l('load_more'); ?></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6" id="followed-main">
                                    <div class="panel_s animated fadeIn">
                                        <div class="panel-body todo-body">
                                            <h4 class="todo-title info-bg">
                                                Followed Lists
                                                <span class="collapse-list">
                                                    <i class="fa fa-sort" aria-hidden="true"></i>
                                                </span>
                                                <div class="sort-list">
                                                    <i class="fa fa-long-arrow-up" aria-hidden="true" style="margin-left: 10px;"></i>
                                                    <i class="fa fa-long-arrow-down" aria-hidden="true"></i>
                                                </div>
                                            </h4>
                                                <ul class="list-unstyled todo finished-todos todos-sortable">
                                                    <li class="padding no-todos hide ui-state-disabled">
                                                        <?php echo _l('no_finished_todos_found'); ?>
                                                    </li>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center padding">
                                                <a href="#" class="btn btn-default text-center finished-loader">
                                                    <?php echo _l('load_more'); ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/todos/_todo.php'); ?>
<?php init_tail(); ?>
<script>
    $(function(){
        public_list();
        unfinished();

        $('#delete_add').on('click', function(e){
            if (confirm_delete()) {
                $.post(admin_url + 'todo/delete_completed/' , function(response) {
                    unfinished();
                });
            }
        });
        $('.view-todo').on('click', function(e){
                unfinished(1, '');
                $(this).hide();
                $('.hide-todo').show();
        });

        $('.hide-todo').on('click', function(e){
                unfinished('', '');
                $(this).hide();
                $('.view-todo').show();
        });

        $('.collapse-list').on('click', function(e){
                if($(this).hasClass('active')){
                    $(this).removeClass('active');
                    $('.public-tasks').removeClass('in');
                } else {
                    $('.public-tasks').addClass('collapse in');
                    $(this).addClass('active');
                }
        });

        $('.personal').on('click', function(e){
                if($(this).hasClass('active')){
                    $(this).removeClass('active');
                    $("#personal-main").css("width","50%");
                    $('#followed-main').show();
                } else {
                    $("#personal-main").css("width","100%");
                    $(this).addClass('active');
                    $('#followed-main').hide();
                    $('#personal-main').show();
                }
        });

        $('.followed').on('click', function(e){
                if($(this).hasClass('active')){
                    $(this).removeClass('active');
                    $("#followed-main").css("width","50%");
                    $('#personal-main').show();
                    
                } else {
                    $("#followed-main").css("width","100%");
                    $(this).addClass('active');
                    $('#personal-main').hide();
                    $('#followed-main').show();
                }
        });

        $('.sort-todo').on('click', function(e){
            if($(this).hasClass('active')){
                $(this).removeClass('active');
                unfinished('', '');
            } else {
                $(this).addClass('active');
                unfinished('', 1);
            }
            
        });

        $('.sort-list').on('click', function(e){
            if($(this).hasClass('active')){
                $(this).removeClass('active');
                public_list('');
            } else {
                $(this).addClass('active');
                public_list(1);
            }
            
        });
    });

    function unfinished(finished = '', sort = ''){
        var total_pages_unfinished = '<?php echo $total_pages_unfinished; ?>';
        var total_pages_finished = '<?php echo $total_pages_finished; ?>';
        var page_unfinished = 0;
        var page_finished = 0;

        console.log(finished);

        if (page_unfinished <= total_pages_unfinished) {
            $.post(window.location.href, {
                finished: finished,
                sort: sort,
                todo_page: page_unfinished
            }).done(function(response) {
                response = JSON.parse(response);
                $('.unfinished-todos').html('');

                if (response.length == 0) {
                    $('.unfinished-todos .no-todos').removeClass('hide');
                    $('.unfinished-todos').append('<li class="padding no-todos hifde ui-state-disabled">No todos found</li>');
                }

                $.each(response, function(i, obj) {
                    if(obj['finished'] == 0){
                        $('.unfinished-todos').append(render_li_items(0, obj));
                    } else {
                        $('.unfinished-todos').append(render_li_items(1, obj));
                    }
                    
                });

                page_unfinished++;
            });

            if (page_unfinished >= total_pages_unfinished - 1) {
                $(".unfinished-loader").addClass("disabled");
            }
        }
    }

    function render_li_items(finished, obj) {
        var todo_finished_class = '';
        var checked = '';
        if (finished == 1) {
            todo_finished_class = ' line-throught';
            checked = 'checked';
        }
        return '<li><div class="media"><div class="media-left no-padding-right"><div class="dragger todo-dragger"></div> <input type="hidden" value="' + finished + '" name="finished"><input type="hidden" value="' + obj.item_order + '" name="todo_order"><div class="checkbox checkbox-default todo-checkbox"><input type="checkbox" name="todo_id" value="' + obj.todoid + '" '+checked+' style="border-radius: 50%;"><label></label></div></div> <div class="media-body"><p class="todo-description' + todo_finished_class + ' no-padding-left">' + obj.description + '<a href="#" onclick="delete_todo_item(this,' + obj.todoid + '); return false;" class="pull-right text-muted"><i class="fa fa-trash-o"></i></a><a href="#" onclick="edit_todo_item('+obj.todoid+'); return false;" class="pull-right text-muted mright5"><i class="fa fa-pencil"></i></a></p><small class="todo-date">' + obj.dateadded + '</small></div></div></li>';
    }

    function public_list(sort = 0){
        requestGetJSON('todo/public_lists/' + sort).done(function(response) {
            var html = '';
            $.each(response['public_folder_listed'], function(i, obj) {
                html += '<li class="public-folder">';
                    html += '<div class="btn-public-folder-header">';
                        html += '<a href="#" onclick="edit_public_folder('+obj["id"]+'); return false;">';
                            html += '<i class="fa fa-pencil public_action"></i>';
                        html += '</a>';
                        html += '<a href="#" onclick="delete_public_folder('+obj["id"]+'); return false;">';
                            html += '<i class="fa fa-trash-o public_action"></i>';
                        html += '</a>'
                    html += '</div>';

                    html += '<h4  class="test collapsed" data-toggle="collapse" data-target="#todo'+obj['id']+'"  style="font-weight: 600;">'+ obj['name'] +'</h4>';
                    html += '<ul class="nav nav-list collapse public-tasks" id="todo'+obj['id']+'">';
                        
                        html += '<h4 class="public_add_tasks" onclick="public_id('+obj['public_folder_id']+', 1); return false;">';
                        html += '<a href="#__todo" data-toggle="modal" style="color: grey !important">';
                        html += '<i class="fa fa-plus-circle" aria-hidden="true"></i>';
                        html += '<span> Add to-do</span>';
                        html += '</a>';
                        html += '</h4>';

                        $.each(response['todos'], function(t, t_obj) {
                            if(obj['public_folder_id'] == t_obj['public_folder_id']){
                                html += '<li id="public_todo'+ t_obj['todoid'] +'">';
                                    html += '<div class="btn-public-folder">';

                                    html += '<a href="#" onclick="edit_todo_item('+t_obj['todoid']+', '+obj['id']+'); return false;">';
                                    html += '<i class="fa fa-pencil public_action" style="color: grey !important"></i>';
                                    html += '</a>';
                                    html += '<a href="#" onclick="delete_todo_item_public('+t_obj['todoid']+'); return false;">';
                                    html += '<i class="fa fa-trash-o public_action" style="color: grey !important"></i>';
                                    html += '</a>';

                                    if(t_obj['staffid'] == 0 ){
                                        html += '<a href="#" onclick="assign_to_me('+t_obj['todoid']+'); return false;" >';
                                        html += '<i class="fa fa-clipboard" aria-hidden="true" style="color: #ff6f00!important"></i>';
                                        html += '</a>';
                                    } else {
                                        html += '<a href="#" onclick="unassign_to_me('+t_obj['todoid']+'); return false;" >';
                                        html += '<i class="fa fa-clipboard" aria-hidden="true" style="color: grey !important"></i>';
                                        html += '</a>';
                                    }

                                    html += '</div>';

                                    if(t_obj['finished'] == 1) {
                                        html += '<h4 style="text-decoration: line-through;">' +t_obj['description'] +'</h4>';
                                    } else {
                                        html += '<h4>' +t_obj['description'] +'</h4>';
                                    }

                                html += '</li>';
                            }
                        });
                    html += '</ul>';

                html += '</li>';
            });

            $('.finished-todos').html(html);
        });
    }

    function public_id(id, public){
        $("#public_folder_id").val(id);
        if(public == 1){
            $("#staffid").val(0);
        }
        
    }
</script>
</body>
</html>
