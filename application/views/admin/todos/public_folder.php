<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<style type="text/css">
    .view_public{
        float: right;
        font-size: 16px;
        font-weight: 800;
        margin-right: 5px;
    }
    .public-folder{
        background-color: #C2E9FC;
        margin: 0px !important;
        padding: 10px 20px;
        color: #0FA9F4;
    }
    .btn-public-folder{
        float: right;
        font-size: 16px;
        font-weight: 800;
        margin-right: 45px;
    }
    .follow{
        border: 1px solid;
        float: right;
        width: 90px;
        text-align: center;
        font-size: 13px;
        font-weight: 100;
        background-color: #fff;
        border-radius: 10px;
        padding: 2px 0px;
        margin-left: 5px;
    }
    .public_action{
        margin-right: 10px;
        font-size: 22px;
    }
    .public-tasks{
        background-color: #fff;
        margin-left: -20px;
        margin-right: -20px;
        color: gray;
    }

    .public-tasks li{
        padding: 5px 20px;
        font-size: 15px;
    }

    .public_add_tasks i{
        color: #03a9f4;
    }

    .public_add_tasks{
        margin-left: 20px;
    }

    .add-public-folder{
        float: right;
        font-size: 22px;
        font-weight: 800;
        margin-right: 25px;
    }

    #unfollow{
        background-color: #B31756;
        color: #fff;
        border-color: #B31756;
    }

    .test:before {
        font-family: 'Glyphicons Halflings';
        content: "\e114";
        color: #008ece;
        font-size: 18px;
        right: 35px;
        position: absolute;
        top: 12px;
    }
    .test.collapsed:before {
        content: "\e080";
    }

    .tasks-title{
        margin-bottom: 0px;
        margin-top: 0px;
    }

    .owner{
        margin-left: 15px;
        font-size: 12px;
    }

</style>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="clearfix"></div>
                        <div class="_buttons">
                            <a href="<?php echo admin_url('todo/'); ?>" class="view_public">Personal View</a>
                            <h3 class="main_headers"> Public To-do Lists </h3>
                        </div>
                        
                        <hr class="hr-panel-heading" />
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="panel_s animated fadeIn">
                                        <div class="panel-body todo-body">
                                            <div class="add-public-folder">
                                                <a href="#__public_folder" data-toggle="modal" style="color: #fff !important">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                            <h4 class="todo-title info-bg" style="font-size: 18px; ">Public Lists</h4>
                                                <ul class="list-unstyled todo finished-todos todos-sortable" id="reloaded">
                                                    <li class="padding no-todos hide ui-state-disabled">
                                                        <?php echo _l('no_finished_todos_found'); ?>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- <div class="col-md-12 text-center padding">
                                                <a href="#" class="btn btn-default text-center finished-loader">
                                                    <?php //echo _l('load_more'); ?>
                                                </a>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/todos/_public_folder.php'); ?>
<?php $this->load->view('admin/todos/_todo.php'); ?>
<?php init_tail(); ?>
<script>
    $(function(){
        public_list();
    });

    function public_list(){
        requestGetJSON('todo/public_lists').done(function(response) {
            var html = '';
            $.each(response['public_folder'], function(i, obj) {
                
                
                html += '<li class="public-folder">';
                    html += '<div class="btn-public-folder">';
                        html += '<a href="#" onclick="edit_public_folder('+obj["id"]+'); return false;">';
                            html += '<i class="fa fa-pencil public_action"></i>';
                        html += '</a>';
                        html += '<a href="#" onclick="delete_public_folder('+obj["id"]+'); return false;">';
                            html += '<i class="fa fa-trash-o public_action"></i>';
                        html += '</a>';

                        if(response['followed_folder'][obj['id']] == undefined){
                            html += '<a href="#" onclick="follow_public_folder('+obj['id']+'); return false;">';
                            html += '<div id="follow" class="follow">Follow</div>';
                            html += '</a>';
                        } else {
                            html += '<a href="#" onclick="unfollow_public_folder('+ response['followed_folder'][obj['id']]['id'] +'); return false;">';
                            html += '<div id="unfollow" class="follow">Unfollow</div>';
                            html += '</a>'; 
                        }

                    html += '</div>';

                    html += '<h4 class="test collapsed"  data-toggle="collapse" data-target="#todo'+obj['id']+'"  style="font-weight: 600;">'+ obj['name'] +'</h4>';
                    html += '<ul class="nav nav-list collapse public-tasks" id="todo'+obj['id']+'">';
                        
                        html += '<h4 class="public_add_tasks" onclick="public_id('+obj['id']+', 1); return false;">';
                        html += '<a href="#__todo" data-toggle="modal" style="color: grey !important">';
                        html += '<i class="fa fa-plus-circle" aria-hidden="true"></i>';
                        html += '<span> Add to-do</span>';
                        html += '</a>';
                        html += '</h4>';

                        $.each(response['todos'], function(t, t_obj) {
                            if(obj['id'] == t_obj['public_folder_id']){
                                html += '<li id="public_todo'+ t_obj['todoid'] +'">';
                                    html += '<div class="btn-public-folder">';

                                    html += '<a href="#" onclick="edit_todo_item('+t_obj['todoid']+', '+obj['id']+'); return false;">';
                                    html += '<i class="fa fa-pencil public_action" style="color: grey !important"></i>';
                                    html += '</a>';
                                    html += '<a href="#" onclick="delete_todo_item_public('+t_obj['todoid']+'); return false;">';
                                    html += '<i class="fa fa-trash-o public_action" style="color: grey !important"></i>';
                                    html += '</a>';

                                    if(t_obj['staffid'] == 0 ){
                                        html += '<a href="#" onclick="assign_to_me('+t_obj['todoid']+'); return false;" >';
                                        html += '<i class="fa fa-clipboard" aria-hidden="true" style="color: #ff6f00!important"></i>';
                                        html += '</a>';
                                    } else {
                                        html += '<a href="#" onclick="unassign_to_me('+t_obj['todoid']+'); return false;" >';
                                        html += '<i class="fa fa-clipboard" aria-hidden="true" style="color: grey !important"></i>';
                                        html += '</a>';
                                    }

                                    html += '</div>';

                                    if(t_obj['finished'] == 1) {
                                        html += '<h4 class="tasks-title" style="text-decoration: line-through;">' +t_obj['description'] +'</h4>';
                                    } else {
                                        html += '<h4 class="tasks-title">' +t_obj['description'] +'</h4>';
                                    }
                                    html += '<span class="owner"> owner: christest </span>'

                                html += '</li>';
                            }
                        });
                    html += '</ul>';

                html += '</li>';
            });

            $('.finished-todos').html(html);
        });
    }

    function public_id(id, public){
        $("#public_folder_id").val(id);
        if(public == 1){
            $("#staffid").val(0);
        }
    }

</script>
</body>
</html>
