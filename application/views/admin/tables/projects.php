<?php

defined('BASEPATH') or exit('No direct script access allowed');

$hasPermissionEdit   = has_permission('projects', '', 'edit');
$hasPermissionDelete = has_permission('projects', '', 'delete');
$hasPermissionCreate = has_permission('projects', '', 'create');

$project_statuses = $this->ci->projects_model->get_project_statuses();

$aColumns = [
    '1', // bulk actions
    db_prefix() . 'projects.id as id',
    'name',
    get_sql_select_client_company(),
    '(SELECT GROUP_CONCAT(name SEPARATOR ",") FROM ' . db_prefix() . 'taggables JOIN ' . db_prefix() . 'tags ON ' . db_prefix() . 'taggables.tag_id = ' . db_prefix() . 'tags.id WHERE rel_id = ' . db_prefix() . 'projects.id and rel_type="project" ORDER by tag_order ASC) as tags',
    'start_date',
    'deadline',
    '(SELECT GROUP_CONCAT(CONCAT(firstname, \' \', lastname) SEPARATOR ",") FROM ' . db_prefix() . 'project_members JOIN ' . db_prefix() . 'staff on ' . db_prefix() . 'staff.staffid = ' . db_prefix() . 'project_members.staff_id WHERE project_id=' . db_prefix() . 'projects.id ORDER BY staff_id) as members',
    'status',
    ];


$sIndexColumn = 'id';
$sTable       = db_prefix() . 'projects';

$join = [
    'JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'projects.clientid',
];

$where  = [];
$filter = [];
array_push($where, 'AND (parent_campaign = 0 OR parent_campaign IS NULL)');

if ($clientid != '') {
    array_push($where, ' AND clientid=' . $this->ci->db->escape_str($clientid));
}

if (!has_permission('projects', '', 'view') || $this->ci->input->post('my_projects')) {
    array_push($where, ' AND ' . db_prefix() . 'projects.id IN (SELECT project_id FROM ' . db_prefix() . 'project_members WHERE staff_id=' . get_staff_user_id() . ')');
}

$statusIds = [];
$parent_campaign = [];


foreach($this->ci->projects_model->check_sub_campaign() as $parent){
    array_push($parent_campaign, $parent['parent_campaign']);
}


foreach ($this->ci->projects_model->get_project_statuses() as $status) {
    if ($this->ci->input->post('project_status_' . $status['id'])) {
        array_push($statusIds, $status['id']);
    }
}

if (count($statusIds) > 0) {
    array_push($filter, 'OR status IN (' . implode(', ', $statusIds) . ')');
}

if (count($filter) > 0) {
    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
}

$custom_fields = get_table_custom_fields('projects');

foreach ($custom_fields as $key => $field) {
    $selectAs = (is_cf_date($field) ? 'date_picker_cvalue_' . $key : 'cvalue_' . $key);
    array_push($customFieldsColumns, $selectAs);
    array_push($aColumns, 'ctable_' . $key . '.value as ' . $selectAs);
    array_push($join, 'LEFT JOIN ' . db_prefix() . 'customfieldsvalues as ctable_' . $key . ' ON ' . db_prefix() . 'projects.id = ctable_' . $key . '.relid AND ctable_' . $key . '.fieldto="' . $field['fieldto'] . '" AND ctable_' . $key . '.fieldid=' . $field['id']);
}

$aColumns = hooks()->apply_filters('projects_table_sql_columns', $aColumns);

// Fix for big queries. Some hosting have max_join_limit
if (count($custom_fields) > 4) {
    @$this->ci->db->query('SET SQL_BIG_SELECTS=1');
}

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
    'clientid',
    '(SELECT GROUP_CONCAT(staff_id SEPARATOR ",") FROM ' . db_prefix() . 'project_members WHERE project_id=' . db_prefix() . 'projects.id ORDER BY staff_id) as members_ids',
]);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];

    $link = admin_url('projects/view/' . $aRow['id']);

    $row[] = '<div class="checkbox"><input type="checkbox" value="' . $aRow['id'] . '"><label></label></div>';

    $row[] = '<a href="' . $link . '">' . $aRow['id'] . '</a>';

    if (in_array($aRow['id'], $parent_campaign)) {
        $name = '<div class="accordion-toggle collapsed" data-toggle="collapse" data-target=".multi-collapse'.$aRow['id'].'" aria-controls="sub_status'.$aRow['id'].' subCam'.$aRow['id'].' parent'.$aRow['id'].' start_date'.$aRow['id'].' end_date'.$aRow['id'].'" style="z-index: 1; position: relative; ">';

        $name .= '<a href="' . $link . '">' . $aRow['name'] . '</a>';

        $name .= '<div class="row-options" data-id="'.$aRow['id'].'" data-child="1">';

        $name .= '<a href="' . $link . '" >' . _l('view') . '</a>';

        if ($hasPermissionCreate && !$clientid) {
            $name .= ' | <a href="#" onclick="copy_project(' . $aRow['id'] . ');return false;">' . _l('copy_project') . '</a>';
        }

        if ($hasPermissionEdit) {
            $name .= ' | <a href="' . admin_url('projects/project/' . $aRow['id']) . '">' . _l('edit') . '</a>';
        }

        if ($hasPermissionDelete) {
            $name .= ' | <a href="' . admin_url('projects/delete/' . $aRow['id']) . '" class="text-danger _delete">' . _l('delete') . '</a>';
        }

        $name .= '</div>';
        $name .= '</div>';
        $name .= '<div id="subCam'.$aRow['id'].'" class="collapse multi-collapse'.$aRow['id'].'"></div>';
        
    } else {
        $name = '<a href="' . $link . '">' . $aRow['name'] . '</a>';

        $name .= '<div class="row-options" data-id="'.$aRow['id'].'" data-child="0">';

        $name .= '<a href="' . $link . '" >' . _l('view') . '</a>';

        if ($hasPermissionCreate && !$clientid) {
            $name .= ' | <a href="#" onclick="copy_project(' . $aRow['id'] . ');return false;">' . _l('copy_project') . '</a>';
        }

        if ($hasPermissionEdit) {
            $name .= ' | <a href="' . admin_url('projects/project/' . $aRow['id']) . '">' . _l('edit') . '</a>';
        }

        if ($hasPermissionDelete) {
            $name .= ' | <a href="' . admin_url('projects/delete/' . $aRow['id']) . '" class="text-danger _delete">' . _l('delete') . '</a>';
        }

        $name .= '</div>';
    }

    

    $row[] = $name;

    $partner = '<a href="' . admin_url('clients/client/' . $aRow['clientid']) . '">' . $aRow['company'] . '</a>';
    $partner .= '<div id="partner'.$aRow['id'].'" class="collapse multi-collapse'.$aRow['id'].'" style="margin-top: 45px;"></div>';

    $row[] = $partner;
    

    $row[] = render_tags($aRow['tags']);


        //CONVERT DATE TIME
    // var_dump($timezone['user_timezone']);
    $datetime = new DateTime(_d($aRow['start_date']));
    $my_timezone = new DateTimeZone($timezone['user_timezone']);
    $datetime->setTimezone($my_timezone);
    $start_date = $datetime->format('dMY H:i:s');

    $datetime1 = new DateTime(_d($aRow['deadline']));
    $my_timezone1 = new DateTimeZone($timezone['user_timezone']);
    $datetime1->setTimezone($my_timezone1);
    $deadline = $datetime1->format('dMY H:i:s');


    $launch_date = $start_date;
    $launch_date .= '<div id="start_date'.$aRow['id'].'" class="collapse multi-collapse'.$aRow['id'].'" style="margin-top: 45px;"> </div>';

    $row[] = $launch_date;
    
    // $row[] = $deadline;
    if (has_permission('projects', '', 'edit')) {

        $end_date = '<input
                style="border: 0px;"
                name="deadline"
                tabindex="-1"
                value="'.$deadline.'"
                
                data-id="'.$aRow['id'].'"
                class="project-list-createdate pointer "
                autocomplete="off"
                onclick="duedate(' . $aRow['id'] . '); return false;">';
        $end_date .= '<div id="end_date'.$aRow['id'].'" class="collapse multi-collapse'.$aRow['id'].'" style="margin-top: 45px;"></div>';
    } else {
        $end_date = $deadline;
        $end_date .= '<div id="end_date'.$aRow['id'].'" class="collapse multi-collapse'.$aRow['id'].'" style="margin-top: 45px;"></div>';
    }

    $row[] = $end_date;

    $membersOutput = '';

    $members       = explode(',', $aRow['members']);
    $exportMembers = '';
    foreach ($members as $key => $member) {
        if ($member != '') {
            $members_ids = explode(',', $aRow['members_ids']);
            $member_id   = $members_ids[$key];
            $membersOutput .= '<a href="' . admin_url('profile/' . $member_id) . '">' .
            staff_profile_image($member_id, [
                'staff-profile-image-small mright5',
                ], 'small', [
                'data-toggle' => 'tooltip',
                'data-title'  => $member,
                ]) . '</a>';
            // For exporting
            $exportMembers .= $member . ', ';
        }
    }

    $membersOutput .= '<span class="hide">' . trim($exportMembers, ', ') . '</span>';
    $membersOutput .= '<div id="member'.$aRow['id'].'" class="collapse multi-collapse'.$aRow['id'].'" style="margin-top: 30px;"></div>';
    $row[] = $membersOutput;

    $status = get_project_status_by_id($aRow['status']);
    // $sub_status  = '<span class="label label inline-block project-status-' . $aRow['status'] . '" style="color:' . $status['color'] . ';border:1px solid ' . $status['color'] . '">' . $status['name'] . '</span>';

    $sub_status = '<div class="dropdown inline-block mleft5 table-export-exclude">';
    $sub_status .= '<a href="#" style="font-size:14px;vertical-align:middle;" class="dropdown-toggle text-dark" id="tableTaskStatus-' . $aRow['id'] . '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
    $sub_status .= '<span class="label label inline-block project-status-' . $aRow['status'] . '" style="color:' . $status['color'] . ';border:1px solid ' . $status['color'] . '">' . $status['name'] . ' <i class="fa fa-caret-down" aria-hidden="true"></i></span>';
    $sub_status .= '</a>';

    $sub_status .= '<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="tableTaskStatus-' . $aRow['id'] . '">';
    foreach ($project_statuses as $taskChangeStatus) {
        if ($aRow['status'] != $taskChangeStatus['id']) {
            $sub_status .= '<li>
                <a href="" onclick="campaign_mark_as(' . $taskChangeStatus['id'] . ',' . $aRow['id'] . '); return false;">
                ' .$taskChangeStatus['name']. '
                </a>
            </li>';
        }
    }
    $sub_status .= '</ul>';
    $sub_status .= '</div>';

    $sub_status .= '<div id="sub_status'.$aRow['id'].'" class="collapse multi-collapse'.$aRow['id'].'" style="margin-top: 45px;"></div>';

    $row[] = $sub_status;

    // Custom fields add values
    foreach ($customFieldsColumns as $customFieldColumn) {
        $row[] = (strpos($customFieldColumn, 'date_picker_') !== false ? _d($aRow[$customFieldColumn]) : $aRow[$customFieldColumn]);
    }

    $row['DT_RowClass'] = 'has-row-options';

    $row = hooks()->apply_filters('projects_table_row_data', $row, $aRow);

    $output['aaData'][] = $row;
}
