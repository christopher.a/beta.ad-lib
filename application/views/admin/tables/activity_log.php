<?php

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'description',
    'date',
    db_prefix().'activity_log.staffid',
    ];

$sWhere = [];
if ($this->ci->input->post('activity_log_date')) {
    array_push($sWhere, 'AND date LIKE "' . $this->ci->db->escape_like_str(to_sql_date($this->ci->input->post('activity_log_date'))) . '%" ESCAPE \'!\'');
}
$sIndexColumn = 'id';
$sTable       = db_prefix().'activity_log';
$result       = data_tables_init($aColumns, $sIndexColumn, $sTable, [], $sWhere);
$output       = $result['output'];
$rResult      = $result['rResult'];
foreach ($rResult as $aRow) {
    $row = [];
    for ($i = 0; $i < count($aColumns); $i++) {
        $_data = $aRow[$aColumns[$i]];
        if ($aColumns[$i] == 'date') {
            //CONVERT DATE TIME
            $datetime = new DateTime(_dt($_data));
            $my_timezone = new DateTimeZone($user_timezone);
            $datetime->setTimezone($my_timezone);
            $final_date1 = $datetime->format('dMY H:i:s');

            $_data = $final_date1;
        }
        $row[] = $_data;
    }
    $output['aaData'][] = $row;
}
