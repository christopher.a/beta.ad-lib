<?php

defined('BASEPATH') or exit('No direct script access allowed');

$filter = [];

if ($this->ci->input->post('my_tasks')) {
    array_push($filter, 'OR (' . db_prefix() . 'tasks.id IN (SELECT taskid FROM ' . db_prefix() . 'task_assigned WHERE staffid = ' . get_staff_user_id() . '))');
}

$task_statuses = $this->ci->tasks_model->get_statuses();
$_statuses     = [];
foreach ($task_statuses as $status) {
    if ($this->ci->input->post('task_status_' . $status['id'])) {
        array_push($_statuses, $status['id']);
    }
}

//for teams module
$this->ci->load->model('teams/teams_model');
$teams = $this->ci->teams_model->get_teams();
$_teams = [];
foreach ($teams as $team) {
    if ($this->ci->input->post('team_' . $team->teamid)) {
        array_push($_teams, $team->teamid);
    }
}

if (count($_teams) > 0) {
    // $join = [' JOIN '.db_prefix().'task_assigned ON '.db_prefix().'task_assigned.taskid = '.db_prefix().'tasks.id JOIN '.db_prefix().'staff ON '.db_prefix().'staff.staffid = '.db_prefix().'task_assigned.staffid'];
    array_push($filter, 'AND '.db_prefix().'tasks.teamid IN (' . implode(', ', $_teams) . ')');
}
//


if (count($_statuses) > 0) {
    array_push($filter, 'AND status IN (' . implode(', ', $_statuses) . ')');
}
if ($this->ci->input->post('not_assigned')) {
    array_push($filter, 'AND ' . db_prefix() . 'tasks.id NOT IN (SELECT taskid FROM ' . db_prefix() . 'task_assigned)');
}
if ($this->ci->input->post('due_date_passed')) {
    array_push($filter, 'AND (duedate < "' . date('Y-m-d') . '" AND duedate IS NOT NULL) AND status != ' . Tasks_model::STATUS_COMPLETE);
}
if ($this->ci->input->post('recurring_tasks')) {
    array_push($filter, 'AND recurring = 1');
}
if ($this->ci->input->post('today_tasks')) {
    array_push($filter, 'AND startdate = "' . date('Y-m-d') . '"');
}
if ($this->ci->input->post('my_following_tasks')) {
    array_push($filter, 'AND (' . db_prefix() . 'tasks.id IN (SELECT taskid FROM ' . db_prefix() . 'task_followers WHERE staffid = ' . get_staff_user_id() . '))');
}
if ($this->ci->input->post('billable')) {
    array_push($filter, 'AND billable = 1');
}
if ($this->ci->input->post('billed')) {
    array_push($filter, 'AND billed = 1');
}
if ($this->ci->input->post('not_billed')) {
    array_push($filter, 'AND billable =1 AND billed=0');
}
if ($this->ci->input->post('upcoming_tasks')) {
    array_push($filter, 'AND (startdate > "' . date('Y-m-d') . '") AND status != ' . Tasks_model::STATUS_COMPLETE);
}

$assignees  = $this->ci->misc_model->get_tasks_distinct_assignees();
$_assignees = [];
foreach ($assignees as $__assignee) {
    if ($this->ci->input->post('task_assigned_' . $__assignee['assigneeid'])) {
        array_push($_assignees, $__assignee['assigneeid']);
    }
}
if (count($_assignees) > 0) {
    array_push($filter, 'AND (' . db_prefix() . 'tasks.id IN (SELECT taskid FROM ' . db_prefix() . 'task_assigned WHERE staffid IN (' . implode(', ', $_assignees) . ')))');
}

if (!has_permission('tasks', '', 'view')) {
    array_push($where, get_tasks_where_string());
}

$dateFilter = [];

if ($this->ci->input->post('due_yesterday')) {
    $yesterday = date("Y-m-d", strtotime("-1 days"));
    array_push($dateFilter, ' DATE(duedate) = "'.$yesterday.'" ');
}

if ($this->ci->input->post('due_today')) {
    $today = date("Y-m-d");
    array_push($dateFilter, ' DATE(duedate) = "'.$today.'" ');
}

if ($this->ci->input->post('due_tomorrow')) {
    $tomorrow = date("Y-m-d", strtotime("+1 days"));
    array_push($dateFilter, ' DATE(duedate) = "'.$tomorrow.'" ');
}

if(!empty($dateFilter)){
    array_push($filter, 'AND ('. implode('OR ', $dateFilter) .') ');
}

if (count($filter) > 0) {
    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
}



$where = hooks()->apply_filters('tasks_table_sql_where', $where);
