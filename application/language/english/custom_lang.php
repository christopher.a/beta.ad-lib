<?php
# Side Bar
$lang['als_dashboard']             = 'Home';
$lang['als_clients']               = 'Partners';
$lang['projects']                  = 'Campaigns';

# Home Content
$lang['dashboard_options']         = 'Home Options';

# Partner Content
$lang['new_client']                		 = 'New Partner';
$lang['import_customers']          		 = 'Import Partners';
$lang['customers_summary']         		 = 'Partners Summary';
$lang['customers_summary_total']   		 = 'Total Partners';
$lang['active_customers']          		 = 'Active Partners';
$lang['inactive_active_customers'] 		 = 'Inactive Partners';
$lang['clients']                         = 'Partners';
$lang['customers_assigned_to_me']  		 = 'Partners assigned to me';
$lang['customer_profile_details']  		 = 'Partner Details';
$lang['customer_group_add_heading']      = 'Add New Partner Group';
$lang['customer_group_edit_heading']     = 'Edit Partner Group';
$lang['new_customer_group']              = 'New Partner Group';
$lang['partner_group']                   = 'Partner';

# Campaign Content
$lang['new_project']               				= 'New Campaign';
$lang['projects_summary']          				= 'Campaigns Summary';
$lang['project_name']              				= 'Campaign Name';
$lang['project_member']            				= 'Campaign Member';
$lang['home_my_projects']          				= 'My Campaigns';
$lang['project_lowercase']         				= 'campaign';
$lang['project_send_created_email']     		= 'Send campaign created email';
$lang['project_settings']               		= 'Campaign settings';
$lang['project_setting_comment_on_tasks']       = 'comment on campaign tasks';
$lang['hide_tasks_on_main_tasks_table']         = 'Hide campaign tasks on main tasks table (admin area)';
$lang['project_customer']                       = 'Partner';
$lang['project_allow_client_to']                = 'Allow partner to %s';
$lang['copy_project']                           = 'Copy Campaign';

# New Partner

$lang['client_company']                          = 'Partner Node';
$lang['clients_country']   					     = 'Primary Market';
$lang['media_agency']							 = 'Media Agency';
$lang['creative_agency']					     = 'Creative Agency';
$lang['primary_contact']				     	 = 'Primary Contact';

# Add New Task

$lang['task_team']                                = 'Team';
$lang['task_request_type']                        = 'Request Type';

# Teams
$lang['task_client_services']                     = 'Client Services';
$lang['task_campaign_management']                 = 'Campaign Management';
$lang['task_design']                 			  = 'Design';
$lang['task_production']                          = 'Production';
$lang['task_quality_assurance']                   = 'Quality Assurance';
$lang['task_reporting']                           = 'Reporting';
$lang['task_product']                             = 'Product';

# Add New Campaign
$lang['project_start_date']                       = 'Launch Date';
$lang['project_deadline']                         = 'Delivery Date';
$lang['project_original_delivery']				  = 'Original Delivery Date';
$lang['project_campaign_type']				  	  = 'Campaign Type';
$lang['project_channel']				  		  = 'Channel';
$lang['project_io_id']				  			  = 'IO ID';
$lang['project_platform_link']				 	  = 'Platform Link';
$lang['project_facebook']						  = 'Facebook';
$lang['project_display']						  = 'Display';
$lang['project_video']						      = 'Video';

# Status
$lang['project_status_1']                         = 'Not Started';
$lang['project_status_2']                         = 'In CS';
$lang['project_status_3']                         = 'In Design';
$lang['project_status_4']                         = 'Completed';
$lang['project_status_5']                         = 'In QA';
$lang['project_status_6']                         = 'With Client';
$lang['project_status_7']                         = 'Delivered';
$lang['project_status_8']                         = 'Live';
$lang['project_status_9']                         = 'In Production';

#Partner List
$lang['clients_list_company']                     = 'Partner Node';
$lang['customer_groups']                          = 'Partner';


?>