<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Todo_model extends App_Model
{
    public $todo_limit;

    public function __construct()
    {
        parent::__construct();
        $this->todo_limit = hooks()->apply_filters('todos_limit', 20);
    }

    public function setTodosLimit($limit)
    {
        $this->todo_limit = $limit;
    }

    public function getTodosLimit()
    {
        return $this->todo_limit;
    }

    public function get($id = '')
    {
        $this->db->where('staffid', get_staff_user_id());

        if (is_numeric($id)) {
            $this->db->where('todoid', $id);

            return $this->db->get(db_prefix().'todos')->row();
        }

        return $this->db->get(db_prefix().'todos')->result_array();
    }

    public function get_public($id = '')
    {

        // if (is_numeric($id)) {
            $this->db->where('id', $id);

            return $this->db->get('tbl_publicfolder')->row();
        // }

        // return $this->db->get(db_prefix().'todos')->result_array();
    }

    /**
     * Get all user todos
     * @param  boolean $finished is finished todos or not
     * @param  mixed $page     pagination limit page
     * @return array
     */
    public function get_todo_items($finished, $page = '', $sort = '')
    {
        $this->db->select();
        $this->db->from(db_prefix().'todos');
        if($finished != ''){
            $this->db->where('finished', $finished);
        }
        
        $this->db->where('staffid', get_staff_user_id());
        if($sort != ''){
            $this->db->order_by('description', 'asc');
        } else {
            $this->db->order_by('item_order', 'asc');
        }
        
        if ($page != '' && $this->input->post('todo_page')) {
            $position = ($page * $this->todo_limit);
            $this->db->limit($this->todo_limit, $position);
        } else {
            $this->db->limit($this->todo_limit);
        }
        $todos = $this->db->get()->result_array();
        // format date
        $i = 0;
        foreach ($todos as $todo) {
            $todos[$i]['dateadded']    = _dt($todo['dateadded']);
            $todos[$i]['datefinished'] = _dt($todo['datefinished']);
            $todos[$i]['description']  = check_for_links($todo['description']);
            $i++;
        }

        return $todos;
    }

    /**
     * Add new user todo
     * @param mixed $data todo $_POST data
     */
    public function add($data)
    {
        
        if($data['public_folder_id'] == ''){
            $data['public_folder_id'] = 0;
        }

        $staffid = 0;

        if($data['staffid'] == ''){
            $staffid = get_staff_user_id();
        }

        $data['dateadded']   = date('Y-m-d H:i:s');
        $data['description'] = nl2br($data['description']);
        $data['staffid']     = $staffid;
        $this->db->insert(db_prefix().'todos', $data);

        return $this->db->insert_id();
    }

    public function add_public_folder($data)
    {
        $data['dateadded']   = date('Y-m-d H:i:s');
        $data['name'] = $data['name'];
        $this->db->insert('tbl_publicfolder', $data);

        return $this->db->insert_id();
    }

    public function get_public_folder()
    {
        $this->db->select('name, id');
        return $this->db->get('tbl_publicfolder')->result_array();
    }

    public function get_public_folder_listed($sort = 0)
    {
        $this->db->join('tbl_public_folder_follow', 'tbl_public_folder_follow.public_folder_id = tbl_publicfolder.id');
        $this->db->where('tbl_public_folder_follow.staffid', get_staff_user_id());
        if($sort != 0){
            $this->db->order_by('tbl_publicfolder.name', 'asc');
        }
        return $this->db->get('tbl_publicfolder')->result_array();
    }
    
    public function get_todo()
    {
        $this->db->select('todoid, description, staffid, public_folder_id, finished');
        $this->db->where('public_folder_id != 0');
        return $this->db->get('tbltodos')->result_array();
    }

    public function get_followed_folder()
    {
        $this->db->where('staffid', get_staff_user_id());
        $query = $this->db->get('tbl_public_folder_follow')->result_array();

        $result = array();
        foreach($query AS $t) {
            $result[$t['public_folder_id']] = $t;
        }
         
        return $result;
    }

    public function update($id, $data)
    {
        $data['description'] = nl2br($data['description']);

        $this->db->where('todoid', $id);
        $this->db->update(db_prefix().'todos', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }

        return false;
    }

    public function update_public_folder($id, $data)
    {
        $data['name'] = $data['name'];

        $this->db->where('id', $id);
        $this->db->update('tbl_publicfolder', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }

        return false;
    }

    /**
     * Update todo's order / Ajax - Sortable
     * @param  mixed $data todo $_POST data
     */
    public function update_todo_items_order($data)
    {
        for ($i = 0; $i < count($data['data']); $i++) {
            $update = [
                'item_order' => $data['data'][$i][1],
                'finished'   => $data['data'][$i][2],
            ];
            if ($data['data'][$i][2] == 1) {
                $update['datefinished'] = date('Y-m-d H:i:s');
            }
            $this->db->where('todoid', $data['data'][$i][0]);
            $this->db->update(db_prefix().'todos', $update);
        }
    }

    /**
     * Delete todo
     * @param  mixed $id todo id
     * @return boolean
     */
    public function delete_todo_item($id)
    {
        $this->db->where('todoid', $id);
        $this->db->where('staffid', get_staff_user_id());
        $this->db->delete(db_prefix().'todos');
        if ($this->db->affected_rows() > 0) {
            return true;
        }

        return false;
    }

    public function delete_public_folder($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_publicfolder');
        if ($this->db->affected_rows() > 0) {
            return true;
        }

        return false;
    }
    
    public function unfollow_public_folder($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_public_folder_follow');
        if ($this->db->affected_rows() > 0) {
            return true;
        }

        return false;
    }

    public function follow_public_folder($id)
    {
        $data['public_folder_id'] = $id;
        $data['staffid']     = get_staff_user_id();
        $this->db->insert('tbl_public_folder_follow', $data);

        return $this->db->insert_id();
    }

    public function assign_to_me($id)
    {
        $data['staffid'] = get_staff_user_id();

        $this->db->where('todoid', $id);
        $this->db->update(db_prefix().'todos', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
    }

    public function unassign_to_me($id)
    {
        $data['staffid'] = 0;

        $this->db->where('todoid', $id);
        $this->db->update(db_prefix().'todos', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
    }

    public function delete_completed()
    {
        $this->db->where('staffid',get_staff_user_id());
        $this->db->where('finished', 1);
        $this->db->delete('tbltodos');
        if ($this->db->affected_rows() > 0) {
            return true;
        }

        return false;
    }

    /**
     * Change todo status / finished or not finished
     * @param  mixed $id     todo id
     * @param  integer $status can be passed 1 or 0
     * @return array
     */
    public function change_todo_status($id, $status)
    {
        $this->db->where('todoid', $id);
        $this->db->where('staffid', get_staff_user_id());
        $date = date('Y-m-d H:i:s');
        $this->db->update(db_prefix().'todos', [
            'finished'     => $status,
            'datefinished' => $date,
        ]);
        if ($this->db->affected_rows() > 0) {
            return [
                'success' => true,
            ];
        }

        return [
            'success' => false,
        ];
    }
}
